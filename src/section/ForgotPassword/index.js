import * as React from 'react'
import Box from '@mui/material/Box'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepLabel from '@mui/material/StepLabel'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'
import { FormControl, Grid, InputLabel, OutlinedInput } from '@mui/material'
import MainCard from '../../ui-component/MainCard'
import { useState } from 'react'
import UserAPI from '../../api/authentication/api'
import { useNavigate } from 'react-router-dom'
import StorageUtil, { STORAGE_KEY } from '../../util/storage'

const steps = ['Địa chỉ Email', 'Mã xác thực', 'Mật khẩu mới', 'Hoàn thành']

const ForgotPassword = () => {
  const navigate = useNavigate()

  const [activeStep, setActiveStep] = useState(0)
  const [inputValue, setInputValue] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const handleNext = async () => {
    switch (activeStep) {
      case 0:
        await UserAPI.forgotPassword({ email: inputValue })
        setInputValue('')
        break
      case 1:
        const res = await UserAPI.verifyCode({ code: inputValue })
        setInputValue('')
        StorageUtil.set(STORAGE_KEY.JWT, res.data)
        break
      case 2:
        await UserAPI.resetPassword({
          confirmNewPassword: newPassword,
          newPassword: confirmPassword,
        })
        break
      case 3:
        navigate('/')
        break
      default:
        break
    }
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  const renderStep = () => {
    switch (activeStep) {
      case 0:
        return (
          <Box width="70%">
            <FormControl fullWidth>
              <InputLabel htmlFor="outlined-adornment-email-login">
                Email
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-email-login"
                type="email"
                name="email"
                label="Email"
                required
                value={inputValue}
                onChange={(e) => setInputValue(e.target.value)}
              />
            </FormControl>
          </Box>
        )
      case 1:
        return (
          <Box width="70%">
            <Typography>Kiểm tra Email của bạn để nhận mã xác thực</Typography>
            <Box>
              <FormControl fullWidth>
                <InputLabel htmlFor="outlined-adornment-email-login">
                  Mã xác thực
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-email-login"
                  type="text"
                  name="Mã xác thực"
                  label="Mã xác thực"
                  required
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)}
                />
              </FormControl>
            </Box>
          </Box>
        )
      case 2:
        return (
          <Box width="70%">
            <Box>
              <FormControl fullWidth>
                <InputLabel htmlFor="outlined-adornment-email-login">
                  Mật khẩu mới
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-email-login"
                  type="password"
                  name="Mật khẩu mới"
                  label="Mật khẩu mới"
                  required
                  value={newPassword}
                  onChange={(e) => setNewPassword(e.target.value)}
                />
              </FormControl>
            </Box>

            <Box mt={2}>
              <FormControl fullWidth>
                <InputLabel htmlFor="outlined-adornment-email-login">
                  Nhập lại mật khẩu mới
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-email-login"
                  type="password"
                  name="Nhập lại mật khẩu mới"
                  label="Nhập lại mật khẩu mới"
                  required
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              </FormControl>
            </Box>
          </Box>
        )
      case 3:
        return (
          <Box>
            <Typography variant="h3">Đã Hoàn Thành</Typography>
          </Box>
        )
      default:
        return (
          <Box>
            <Typography>{activeStep}</Typography>
          </Box>
        )
    }
  }

  return (
    <Box>
      <head>
        <title>Forgot Password</title>
      </head>
      <Grid
        container
        direction="column"
        justifyContent="flex-end"
        sx={{ minHeight: '100vh' }}
      >
        <Grid item xs={12}>
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            sx={{ minHeight: 'calc(100vh - 68px)' }}
          >
            <Grid item sx={{ m: { xs: 1, sm: 3 }, mb: 0 }}>
              <MainCard sx={{ width: '50vw' }}>
                <Box sx={{ width: '100%' }}>
                  <Stepper activeStep={activeStep}>
                    {steps.map((label, index) => {
                      const stepProps = {}
                      const labelProps = {}
                      return (
                        <Step key={label} {...stepProps}>
                          <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                      )
                    })}
                  </Stepper>
                  <React.Fragment>
                    <Box
                      mt={4}
                      sx={{ display: 'flex', justifyContent: 'center' }}
                    >
                      {renderStep()}
                    </Box>
                    <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
                      <Button
                        color="inherit"
                        disabled={activeStep === 0}
                        onClick={handleBack}
                        sx={{ mr: 1 }}
                      >
                        Trở lại
                      </Button>
                      <Box sx={{ flex: '1 1 auto' }} />

                      <Button onClick={handleNext}>
                        {activeStep === steps.length - 1
                          ? 'Hoàn thành'
                          : 'Tiếp Tục'}
                      </Button>
                    </Box>
                  </React.Fragment>
                </Box>
              </MainCard>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  )
}

export default ForgotPassword
