import LocationOnIcon from '@mui/icons-material/LocationOn'
import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import StoreAPI from '../../api/store/api'
import { colors } from '../../app/constant'

const ShopNearby = () => {
  const navigate = useNavigate()
  const [listDistanceStore, setListDistanceStore] = useState([])

  const fetchListDistanceStore = async ({ x, y }) => {
    const data = await StoreAPI.getDistanceStore({ x, y, radius: 1 })
    setListDistanceStore(data.data)
  }

  const getLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition)
    }
  }

  function showPosition(position) {
    fetchListDistanceStore({
      x: position.coords.latitude,
      y: position.coords.longitude,
    })
  }

  useEffect(() => {
    getLocation()
  }, [])

  return (
    listDistanceStore.length > 0 && (
      <Grid container sx={{ padding: { xs: 2, sm: 4 } }}>
        <Grid item xs={12}>
          <Stack>
            <Typography variant="h5" fontWeight={600}>
              Cửa hàng gần đây
            </Typography>
            <Box
              sx={{
                width: '100px',
                backgroundColor: colors.mainColor,
                height: '4px',
              }}
            ></Box>
          </Stack>
        </Grid>

        <Grid item xs={12} mt={2}>
          <Grid container spacing={2}>
            {listDistanceStore?.map((item, index) => (
              <Grid
                item
                xs={6}
                md={4}
                lg={3}
                sx={{
                  display: 'flex',
                }}
                key={index}
              >
                <Card
                  sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    flexDirection: 'column',
                    width: '100%',
                  }}
                  onClick={() => navigate(`store/${item.id}`)}
                >
                  <CardActionArea
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      flex: 1,
                    }}
                  >
                    <CardMedia
                      sx={{
                        height: { xs: '10rem', sm: '15rem' },
                        width: '100%',
                      }}
                      image={item.logo}
                      title={item.name}
                    />
                    <CardContent
                      sx={{
                        width: '100%',
                        flex: 1,
                        display: 'flex',
                        flexDirection: 'column',
                      }}
                    >
                      <Typography
                        variant="h5"
                        sx={{
                          overflow: 'hidden',
                          textOverflow: 'ellipsis',
                          display: '-webkit-box',
                          WebkitLineClamp: '2',
                          WebkitBoxOrient: 'vertical',
                        }}
                      >
                        {item.name}
                      </Typography>
                      <Stack direction="row" flex={1}>
                        <LocationOnIcon
                          sx={{
                            color: colors.mainColor,
                            marginLeft: '-5px',
                          }}
                        />
                        <Typography variant="body2" color="text.secondary">
                          {`${item.street}`}
                        </Typography>
                      </Stack>
                      <Typography
                        variant="body1"
                        sx={{
                          backgroundColor: colors.mainColor,
                          color: colors.whiteColor,
                          px: '6px',
                          py: '2px',
                          borderRadius: 2,
                          marginTop: 'auto',
                          width: 'fit-content',
                        }}
                      >
                        {`${item?.distance} km`}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    )
  )
}

export default ShopNearby
