import {
  Box,
  Button,
  Card,
  CardContent,
  CardMedia,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  List,
  ListItem,
  ListItemButton,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import VoucherAPI from '../../api/voucher/api'
import EmptyData from '../../ui-component/EmptyData'
import { formatPrice } from '../../util/helpers'

const SimpleDialog = (props) => {
  const { onClose, selectedValue, open, data } = props
  const [listVoucher, setListVoucher] = useState()

  const fetchListVoucher = async () => {
    const listProduct = data[data.shopId].map((item) => {
      return {
        amount: item.amount,
        productId: item.id,
      }
    })

    const paramFetchData = {
      orderDetails: listProduct,
      shopId: data.shopId,
    }

    const list = await VoucherAPI.getListVoucherBill(paramFetchData)

    setListVoucher(list.data)
  }

  useEffect(() => {
    fetchListVoucher()
  }, [])

  const handleClose = () => {
    onClose(selectedValue)
  }

  const handleListItemClick = (value) => {
    onClose(value)
  }

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="sm" fullWidth>
      <DialogTitle>List Voucher</DialogTitle>
      <DialogContent>
        <List sx={{ pt: 0 }}>
          {listVoucher?.length > 0 ? (
            listVoucher?.map((item) => (
              <ListItem disableGutters>
                <ListItemButton
                  onClick={() => handleListItemClick(item)}
                  key={item.id}
                >
                  <Card sx={{ display: 'flex', flex: 1 }}>
                    <CardMedia
                      component="img"
                      sx={{ width: 100, objectFit: 'cover' }}
                      image={item.image}
                      alt="Live from space album cover"
                    />
                    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                      <CardContent sx={{ flex: '1 0 auto' }}>
                        <Typography component="div" variant="h5">
                          {item.name}
                        </Typography>
                        <Typography
                          variant="subtitle1"
                          color="text.secondary"
                          component="div"
                        >
                          {item.voucherType === 'PRICE'
                            ? `Giảm giá ${formatPrice(
                                item.valueDiscount || 0
                              )}đ cho đơn hàng từ ${formatPrice(
                                item.valueNeed || 0
                              )}đ`
                            : `Giảm giá ${
                                item.valueDiscount
                              }%, tối đa ${formatPrice(
                                item.maxDiscount || 0
                              )} cho đơn hàng từ ${formatPrice(
                                item.valueNeed || 0
                              )}đ`}
                        </Typography>
                      </CardContent>
                    </Box>
                  </Card>
                </ListItemButton>
              </ListItem>
            ))
          ) : (
            <EmptyData />
          )}
        </List>
      </DialogContent>

      <DialogActions>
        <Button onClick={() => handleClose()}>Đóng</Button>
      </DialogActions>
    </Dialog>
  )
}

export default SimpleDialog
