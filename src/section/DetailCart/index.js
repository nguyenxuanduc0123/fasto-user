import AddIcon from '@mui/icons-material/Add'
import HighlightOffIcon from '@mui/icons-material/HighlightOff'
import ProductionQuantityLimitsOutlinedIcon from '@mui/icons-material/ProductionQuantityLimitsOutlined'
import RemoveIcon from '@mui/icons-material/Remove'
import {
  Box,
  Dialog,
  DialogContent,
  Divider,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router'
import BillAPI from '../../api/bill/api'
import VnpayAPI from '../../api/vnpay/api'
import { colors } from '../../app/constant'
import { useAppSelector } from '../../app/hooks'
import {
  selectListCart,
  selectStatusListCart,
} from '../../features/cart/cartSlice'
import useChangeAmount from '../../hooks/useChangeAmount'
import AppIconButton from '../../ui-component/Button/AppIconButton'
import MainButton from '../../ui-component/Button/MainButton'
import { formatPrice } from '../../util/helpers'
import SimpleDialog from './SimpleDialog'

const DetailCart = ({ data }) => {
  const navigate = useNavigate()
  const { handleChangeAmount } = useChangeAmount()
  const [openModal, setOpenModal] = useState(false)

  const [open, setOpen] = useState(false)
  const [selectedValue, setSelectedValue] = useState()

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = (value) => {
    setOpen(false)
    setSelectedValue(value)
  }

  const listCart = useAppSelector(selectListCart)
  const statusListCart = useAppSelector(selectStatusListCart)

  const totalPrice = (listCart) => {
    const total = listCart[data.shopId]?.reduce((acc, item) => {
      return (acc += item.price * item.amount)
    }, 0)
    return total
  }

  const handleSubmit = async () => {
    const listProduct = listCart[data.shopId]?.map((item) => {
      return {
        amount: item.amount,
        productId: item.id,
      }
    })

    const formData = {
      productDetailRequestDtos: listProduct,
      shopId: data.shopId,
      voucherId: selectedValue?.id || null,
    }

    const res = await BillAPI.createBill(formData)

    if (res.data.billId) {
      const payment = {
        bankCode: 'NCB',
        billId: res.data.billId,
        vnpLocale: 'VN',
        vnpOrderType: `bill ${res.data.billId}`,
        vnpReturnUrl: 'http://safefood.fun/succeed',
      }

      const resVnpay = await VnpayAPI.payment(payment)

      window.open(resVnpay.data.url, '_self')
    }
  }

  const calculateDiscount = () => {
    switch (selectedValue?.voucherType) {
      case 'PRICE':
        return selectedValue?.valueDiscount
      case 'PERCENT':
        const discountPrice =
          totalPrice(listCart) * (selectedValue?.valueDiscount / 100)
        if (discountPrice > selectedValue.maxDiscount) {
          return selectedValue.maxDiscount
        }
        return discountPrice
      default:
        return 0
    }
  }

  useEffect(() => {
    if (!listCart[data.shopId]?.length > 0 && statusListCart.success) {
      setOpenModal(true)
    }
  }, [listCart])

  return (
    <Box>
      <Typography variant="h6" fontWeight={600} mb={1}>
        Chi tiết đơn hàng
      </Typography>

      {listCart[data.shopId]?.map((item) => (
        <Box
          mt={2}
          maxHeight="65vh"
          sx={{
            overflow: 'hidden',
            overflowY: 'auto',
          }}
        >
          <Box display="flex" alignItems="center" mb={1}>
            <Box width="15%" mr={1}>
              <img
                src={item?.image}
                alt=""
                className="object-cover h-[80px] rounded"
              />
            </Box>
            <Stack flex={1}>
              <Typography
                variant="h6"
                sx={{
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                  display: '-webkit-box',
                  WebkitLineClamp: '2',
                  WebkitBoxOrient: 'vertical',
                }}
              >
                {item?.name}
              </Typography>
              <Typography variant="h6">{`${formatPrice(
                item?.price * item?.amount || 0
              )} đ`}</Typography>
            </Stack>

            <Box display="flex" alignItems="center" mr={2}>
              <AppIconButton
                onClick={() => {
                  handleChangeAmount(item, -1)
                }}
              >
                <RemoveIcon sx={{ color: colors.whiteColor }} />
              </AppIconButton>
              <Typography mx={2}>{item?.amount}</Typography>
              <AppIconButton
                onClick={() => {
                  handleChangeAmount(item, 1)
                }}
              >
                <AddIcon sx={{ color: colors.whiteColor }} />
              </AppIconButton>
            </Box>
          </Box>
          <Divider sx={{ my: 2 }} />
        </Box>
      ))}

      <Typography variant="h6" fontWeight={600} mb={1}>
        Voucher
      </Typography>

      <Box ml={2}>
        <Stack direction="row" justifyContent="space-between">
          <Typography variant="body1" fontWeight={600}>
            Áp dụng mã giảm giá:
          </Typography>
          <Typography
            onClick={handleClickOpen}
            fontWeight={600}
            sx={{ cursor: 'pointer', color: colors.mainColor }}
          >
            Chọn voucher
          </Typography>
          <SimpleDialog
            selectedValue={selectedValue}
            open={open}
            onClose={handleClose}
            data={data}
          />
        </Stack>

        {selectedValue && (
          <Stack direction="row" justifyContent="flex-end">
            <Stack direction="row">
              <HighlightOffIcon
                onClick={() => setSelectedValue(null)}
                sx={{ cursor: 'pointer' }}
              />

              <Typography
                variant="body1"
                fontWeight={600}
                sx={{ color: colors.greenColor }}
                ml={1}
              >
                {selectedValue?.name}
              </Typography>
            </Stack>
          </Stack>
        )}

        <Stack direction="row" justifyContent="space-between">
          <Typography variant="body1" fontWeight={600}>
            Tổng (tạm tính):
          </Typography>
          <Typography variant="body1" fontWeight={600}>
            {formatPrice(totalPrice(listCart) || 0)} đ
          </Typography>
        </Stack>

        <Stack direction="row" justifyContent="space-between">
          <Typography variant="body1" fontWeight={600}>
            Giảm giá:
          </Typography>
          <Typography variant="body1" fontWeight={600} color="red">
            -{' '}
            {selectedValue?.id
              ? formatPrice(calculateDiscount(selectedValue) || 0)
              : formatPrice(0)}{' '}
            đ
          </Typography>
        </Stack>

        <Divider sx={{ my: 2 }} />

        <Stack direction="row" justifyContent="space-between">
          <Typography variant="body1" fontWeight={600}>
            Tổng thanh toán:
          </Typography>
          <Typography variant="body1" fontWeight={600}>
            {formatPrice(
              totalPrice(listCart) - calculateDiscount(selectedValue) || 0
            )}{' '}
            đ
          </Typography>
        </Stack>
      </Box>

      <Stack direction="row" justifyContent="center" mt={2}>
        <MainButton onClick={handleSubmit}>Đặt hàng</MainButton>
      </Stack>

      <Dialog
        open={openModal}
        onClose={() => setOpenModal(false)}
        maxWidth="sm"
        fullWidth
      >
        <DialogContent>
          <Stack alignItems="center">
            <ProductionQuantityLimitsOutlinedIcon
              sx={{ fontSize: '40px', color: colors.mainColor }}
            />
            <Typography fontWeight={600} variant="h6">
              Giỏ hàng trống
            </Typography>
            <Typography mb={1}>
              Quay lại trang chủ để tiếp tục mua hàng.
            </Typography>
            <MainButton
              onClick={() => {
                setOpenModal(false)
                navigate('/')
              }}
            >
              Quay lại
            </MainButton>
          </Stack>
        </DialogContent>
      </Dialog>
    </Box>
  )
}

export default DetailCart
