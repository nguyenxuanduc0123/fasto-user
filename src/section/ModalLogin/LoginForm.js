import {
  Box,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Typography,
} from '@mui/material'
import React, { useState } from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import ErrorMessage from '../../ErrorMessage'
import { Visibility, VisibilityOff } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { selectAuthResponse } from '../../features/auth/authSlice'
import { signIn } from '../../features/auth/authAction'
import { useNavigate } from 'react-router-dom'

const LoginForm = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const [showPassword, setShowPassword] = useState(false)

  const response = useAppSelector(selectAuthResponse)

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword)
  }

  const handleMouseDownPassword = (event) => {
    event.preventDefault()
  }

  return (
    <>
      <Grid container direction="column" justifyContent="center" spacing={2}>
        <Grid
          item
          xs={12}
          container
          alignItems="center"
          justifyContent="center"
        >
          <Box sx={{ mb: 2 }}>
            <Typography variant="subtitle1">
              Đăng nhập bằng địa chỉ email để tiếp tục
            </Typography>
          </Box>
        </Grid>
      </Grid>

      <Formik
        initialValues={{
          email: '',
          password: '',
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .email('Must be a valid email')
            .max(255)
            .required('Email is required'),
          password: Yup.string()
            .min(6, 'Should be 8 chars minimum.')
            .max(255)
            .required('Password is required'),
        })}
        onSubmit={async (values, { setStatus }) => {
          try {
            setStatus({ success: true })
            dispatch(
              signIn({
                email: values.email,
                password: values.password,
              })
            )
          } catch (err) {
            console.error(err)
          }
        }}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          touched,
          values,
        }) => (
          <form noValidate onSubmit={handleSubmit}>
            <FormControl
              fullWidth
              error={Boolean(touched.email && errors.email)}
            >
              <InputLabel htmlFor="outlined-adornment-email-login">
                Email
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-email-login"
                type="email"
                value={values.email}
                name="email"
                onBlur={handleBlur}
                onChange={handleChange}
                label="Email"
                inputProps={{}}
              />
              <ErrorMessage name="email" />
            </FormControl>

            <FormControl
              fullWidth
              error={Boolean(touched.password && errors.password)}
              sx={{ mt: 2 }}
            >
              <InputLabel htmlFor="outlined-adornment-password-login">
                Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password-login"
                type={showPassword ? 'text' : 'password'}
                value={values.password}
                name="password"
                onBlur={handleBlur}
                onChange={handleChange}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                      size="large"
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
                inputProps={{}}
              />
              <ErrorMessage name="password" />
            </FormControl>

            {response.error && (
              <FormHelperText error>{response.error}</FormHelperText>
            )}

            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
              mt={1}
            >
              <Typography
                onClick={() => navigate('/forgotPassword')}
                sx={{ cursor: 'pointer' }}
              >
                Quên mật khẩu?
              </Typography>
            </Box>

            <Box sx={{ mt: 2 }}>
              <LoadingButton
                disableElevation
                disabled={response.status === 'pending'}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
              >
                Đăng nhập
              </LoadingButton>
            </Box>
          </form>
        )}
      </Formik>
    </>
  )
}

export default LoginForm
