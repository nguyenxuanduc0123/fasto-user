import {
  Avatar,
  Box,
  Dialog,
  DialogContent,
  Divider,
  Grid,
  Stack,
  Typography,
} from '@mui/material'
import { gapi } from 'gapi-script'
import React, { useEffect } from 'react'
import GoogleLogin from 'react-google-login'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { signInGoogle } from '../../features/auth/authAction'
import {
  closeAllModal,
  selectLoginModal,
} from '../../features/modalLogin/modalLoginSlice'
import LoginForm from './LoginForm'

const ModalLogin = () => {
  const dispatch = useAppDispatch()
  const loginModal = useAppSelector(selectLoginModal)

  const clientId = process.env.REACT_APP_CLIENT_ID_GOOGLE

  const onSuccess = (res) => {
    const accessToken = res.tokenObj.access_token
    const tokenType = res.tokenObj.token_type

    dispatch(
      signInGoogle({
        accessToken,
        tokenType,
      })
    )
  }

  const onFailure = () => {}

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: clientId,
        scope: '',
      })
    }

    gapi.load('client:auth2', start)
  })

  return (
    <Dialog
      open={loginModal}
      onClose={() => dispatch(closeAllModal())}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <Grid container alignItems="center" justifyContent="center">
          <Grid item>
            <Avatar
              alt="Logo"
              src="images/logo2.png"
              sx={{
                width: '80px',
                height: '80px',
                marginBottom: '16px',
              }}
            />
          </Grid>

          <Grid item xs={12}>
            <Box display="flex" alignItems="center">
              <Divider sx={{ flexGrow: 1 }} orientation="horizontal" />
            </Box>
          </Grid>

          <Grid item xs={12} mt={2}>
            <Grid container alignItems="center" justifyContent="center">
              <Grid item>
                <Stack alignItems="center" justifyContent="center" spacing={1}>
                  <Typography gutterBottom variant={'h5'}>
                    Chào mừng quay trở lại
                  </Typography>
                </Stack>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <LoginForm />
          </Grid>

          <Grid item xs={12} textAlign="center" my={1}>
            <Typography>Hoặc</Typography>
          </Grid>

          <Grid
            item
            xs={12}
            sx={{
              '& button': {
                width: '100%',
                display: 'flex',
                justifyContent: 'center',
              },
            }}
          >
            <GoogleLogin
              clientId={clientId}
              buttonText="Đăng nhập bằng Google"
              onSuccess={onSuccess}
              onFailure={onFailure}
              cookiePolicy="single_host_origin"
            />
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  )
}

export default ModalLogin
