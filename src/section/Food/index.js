import AttachMoneyIcon from '@mui/icons-material/AttachMoney'
import StarIcon from '@mui/icons-material/Star'
import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  getListProduct,
  selectListProduct,
} from '../../features/product/productSlice'
import { formatPrice } from '../../util/helpers'
import StoreMallDirectoryOutlinedIcon from '@mui/icons-material/StoreMallDirectoryOutlined'
import { useNavigate } from 'react-router-dom'
import { colors } from '../../app/constant'
const Food = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const listProduct = useAppSelector(selectListProduct)

  const fetchDataList = () => {
    dispatch(getListProduct({ query: '' }))
  }

  useEffect(() => {
    fetchDataList()
  }, [])

  return (
    <Grid container sx={{ padding: { xs: 2, sm: 4 } }}>
      <Grid item xs={12}>
        <Stack>
          <Typography variant="h5" fontWeight={600}>
            Danh sách sản phẩm
          </Typography>
          <Box
            sx={{
              width: '100px',
              backgroundColor: colors.mainColor,
              height: '4px',
            }}
          ></Box>
        </Stack>
      </Grid>

      <Grid item xs={12} mt={2}>
        <Grid container spacing={2}>
          {listProduct?.content?.map((item, index) => (
            <Grid
              key={index}
              item
              xs={6}
              md={4}
              lg={3}
              sx={{
                display: 'flex',
              }}
            >
              <Card
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  flexDirection: 'column',
                  width: '100%',
                }}
                onClick={() => navigate(`store/${item.shopId}`)}
              >
                <CardActionArea
                  sx={{ display: 'flex', flexDirection: 'column', flex: 1 }}
                >
                  <CardMedia
                    sx={{ height: { xs: '10rem', sm: '15rem' }, width: '100%' }}
                    image={item.image}
                    title={item.name}
                  />
                  <CardContent
                    sx={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      flexDirection: 'column',
                      width: '100%',
                      flex: 1,
                      rowGap: 0.5,
                    }}
                  >
                    <Typography
                      variant="h6"
                      sx={{
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        display: '-webkit-box',
                        WebkitLineClamp: '2',
                        WebkitBoxOrient: 'vertical',
                      }}
                    >
                      {item.name}
                    </Typography>

                    <Box
                      sx={{
                        marginTop: 'auto',
                      }}
                      display="flex"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <Box display="flex" alignItems="center">
                        <StoreMallDirectoryOutlinedIcon
                          sx={{
                            color: colors.mainColor,
                            fontSize: '20px',
                          }}
                        />
                        <Typography variant="body1" ml={0.5}>
                          {item.shopName}
                        </Typography>
                      </Box>
                    </Box>

                    <Stack direction="row" spacing={1}>
                      <Typography
                        variant="body2"
                        sx={{
                          backgroundColor: colors.mainColor,
                          color: colors.whiteColor,
                          px: '6px',
                          py: '2px',
                          borderRadius: 2,
                        }}
                      >
                        {item.categoryName}
                      </Typography>
                      <Typography
                        variant="body2"
                        sx={{
                          backgroundColor: colors.blueColor,
                          color: colors.whiteColor,
                          px: '6px',
                          py: '2px',
                          borderRadius: 2,
                        }}
                      >
                        {item.status}
                      </Typography>
                    </Stack>

                    <Box
                      sx={{
                        marginTop: 'auto',
                      }}
                      display="flex"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <Box display="flex" alignItems="center">
                        <AttachMoneyIcon
                          sx={{
                            color: colors.mainColor,
                            fontSize: '20px',
                          }}
                        />
                        <Typography variant="h6" fontWeight={500}>
                          {formatPrice(item.price)} đ
                        </Typography>
                      </Box>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  )
}

export default Food
