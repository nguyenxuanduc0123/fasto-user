import { Box, Stack, Typography } from '@mui/material'
import React, { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  getDetailStore,
  selectDetailStore,
} from '../../features/store/storeSlice'
import LocationOnIcon from '@mui/icons-material/LocationOn'
import { colors } from '../../app/constant'

const ShopPlace = ({ shopId }) => {
  const dispatch = useAppDispatch()

  const { shopProfile } = useAppSelector(selectDetailStore)

  const getShopInfo = (shopId) => {
    dispatch(getDetailStore({ id: shopId }))
  }

  useEffect(() => {
    getShopInfo(shopId)
  }, [])

  return (
    <Box>
      <Typography variant="h6" fontWeight={600} mb={1}>
        Nơi đặt hàng
      </Typography>
      <Box display="flex" alignItems="center" mb={1}>
        <Box width="30%" mr={1}>
          <img
            src={shopProfile?.banner}
            alt=""
            className="object-cover h-[100%] rounded"
          />
        </Box>
        <Stack flex={1}>
          <Typography variant="h5" fontWeight={600}>
            {shopProfile?.name}
          </Typography>
          <Typography
            variant="body1"
            sx={{
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              display: '-webkit-box',
              WebkitLineClamp: '1',
              WebkitBoxOrient: 'vertical',
            }}
          >{`${shopProfile?.streetAddress || ''}, ${
            shopProfile?.stateProvince
          }, ${shopProfile?.city}`}</Typography>

          <Box
            display="flex"
            alignItems="center"
            onClick={() =>
              window.open(
                `https://maps.google.com?q=${shopProfile.x},${shopProfile.y}`,
                '_blank'
              )
            }
            sx={{ cursor: 'pointer' }}
          >
            <LocationOnIcon
              sx={{
                color: colors.mainColor,
              }}
            />
            <Typography ml={0.5}>Xem bản đồ</Typography>
          </Box>
        </Stack>
      </Box>
    </Box>
  )
}

export default ShopPlace
