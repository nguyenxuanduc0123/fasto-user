import CameraAltIcon from '@mui/icons-material/CameraAlt'
import DateRangeOutlinedIcon from '@mui/icons-material/DateRangeOutlined'
import {
  Avatar,
  Badge,
  Box,
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@mui/material'
import { MobileDatePicker } from '@mui/x-date-pickers'
import { IconCalendarEvent, IconMan, IconUser } from '@tabler/icons'
import { format } from 'date-fns'
import { useFormik } from 'formik'
import React, { useEffect, useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  changeAvatar,
  editAccount,
  getAccount,
  selectAccount,
} from '../../features/account/accountSlice'
import AppTextField from '../../ui-component/AppTextField'
import MainCard from '../../ui-component/MainCard'

const AccountInfo = () => {
  const dispatch = useAppDispatch()
  const [isEdit, setIsEdit] = useState(false)
  const [showSelectDateFrom, setShowSelectDateFrom] = useState(false)

  const accountInfo = useAppSelector(selectAccount)

  const fetchAccount = () => {
    dispatch(getAccount())
  }

  const handleChangeAvatar = (img) => {
    dispatch(changeAvatar({ imageUser: img }))
  }

  useEffect(() => {
    if (!accountInfo) {
      fetchAccount()
    }
  }, [])

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      firstName: accountInfo?.firstName,
      lastName: 'a',
      gender: accountInfo?.gender,
      birthday: new Date(accountInfo?.birthday).getTime(),
      userImage: accountInfo?.userImage,
    },
    onSubmit: (values) => {
      const formData = convertValue(values)
      try {
        dispatch(editAccount(formData))
      } catch (err) {
        console.error(err)
      }
    },
  })

  const convertValue = (values) => {
    const date = values.birthday / 1000
    return {
      ...values,
      birthday: date,
    }
  }

  return (
    <MainCard>
      <Box component="form" onSubmit={formik.handleSubmit} noValidate>
        <Box mb={2} sx={{ textAlign: 'center' }}>
          <Badge
            overlap="circular"
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            badgeContent={<CameraAltIcon />}
            sx={{ cursor: 'pointer' }}
          >
            <Avatar
              src={accountInfo?.userImage}
              sx={{ margin: 'auto', width: '100px', height: '100px' }}
            />
          </Badge>
        </Box>
        <Box>
          <Box display="flex" justifyContent="space-between" mb={2}>
            <Typography variant="h6" fontWeight={600}>
              Thông tin cá nhân
            </Typography>
            <Typography
              sx={{ cursor: 'pointer' }}
              onClick={() => {
                if (isEdit) {
                  formik.handleSubmit()
                }
                setIsEdit(!isEdit)
              }}
            >
              {!isEdit ? 'Thay đổi' : 'Lưu'}
            </Typography>
          </Box>

          <Box ml={1}>
            <Box display="flex" alignItems="center" mb={2}>
              <IconUser stroke={1.5} size="1.3rem" />
              {!isEdit && (
                <Typography ml={1}>{accountInfo?.firstName}</Typography>
              )}
              {isEdit && (
                <AppTextField formik={formik} name="firstName" label="Họ tên" />
              )}
            </Box>
            <Box display="flex" alignItems="center" mb={2}>
              <IconMan stroke={1.5} size="1.3rem" />
              {!isEdit && (
                <Typography ml={1}>{accountInfo?.gender || '-'}</Typography>
              )}
              {isEdit && (
                <FormControl fullWidth>
                  <InputLabel
                    error={
                      formik.touched['gender'] &&
                      Boolean(formik.errors['gender'])
                    }
                    id="demo-simple-select-helper-label"
                  >
                    {'Giới tính'}
                  </InputLabel>
                  <Select
                    value={formik.values.gender}
                    label={'Giới tính'}
                    onChange={(event) => {
                      formik.setFieldValue('gender', event.target.value)
                    }}
                    error={
                      formik.touched['gender'] &&
                      Boolean(formik.errors['gender'])
                    }
                    defaultValue={'MALE'}
                  >
                    <MenuItem value={'MALE'}>Nam</MenuItem>
                    <MenuItem value={'FEMALE'}>Nữ</MenuItem>
                  </Select>
                  <FormHelperText error={Boolean(formik.errors['gender'])}>
                    {formik.errors['gender']}
                  </FormHelperText>
                </FormControl>
              )}
            </Box>
            <Box display="flex" alignItems="center" mb={2}>
              <IconCalendarEvent stroke={1.5} size="1.3rem" />
              {!isEdit && (
                <Typography ml={1}>{`${format(
                  new Date(accountInfo?.birthday).getTime() || new Date(),
                  'dd-MM-yyyy'
                )} `}</Typography>
              )}
              {isEdit && (
                <MobileDatePicker
                  ampm={false}
                  label="Birthday"
                  inputFormat="dd-MM-yyyy"
                  value={formik.values.birthday}
                  onChange={(newValue) => {
                    formik.setFieldValue('birthday', newValue.getTime())
                  }}
                  open={showSelectDateFrom}
                  onOpen={() => setShowSelectDateFrom(true)}
                  onClose={() => setShowSelectDateFrom(false)}
                  showToolbar={false}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment
                        position="end"
                        onClick={() => setShowSelectDateFrom(true)}
                      >
                        <IconButton edge="end" size="large" sx={{ mr: '5px' }}>
                          <DateRangeOutlinedIcon />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  renderInput={(params) => (
                    <TextField required fullWidth name="birthday" {...params} />
                  )}
                />
              )}
            </Box>
          </Box>
        </Box>
      </Box>
    </MainCard>
  )
}

export default AccountInfo
