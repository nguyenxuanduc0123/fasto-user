import {
  Avatar,
  Dialog,
  DialogContent,
  Grid,
  Stack,
  Typography,
} from '@mui/material'
import React from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  closeAllModal,
  selectRegisterModal,
} from '../../features/modalLogin/modalLoginSlice'
import RegisterForm from './RegisterForm'
import Verify from './Verify'

const ModalRegister = () => {
  const dispatch = useAppDispatch()
  const registerModal = useAppSelector(selectRegisterModal)

  return (
    <Dialog
      open={registerModal}
      onClose={() => dispatch(closeAllModal())}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <Grid container alignItems="center" justifyContent="center">
          <Grid item>
            <Avatar
              alt="Logo"
              src="images/logo2.png"
              sx={{
                width: '80px',
                height: '80px',
              }}
            />
          </Grid>

          <Grid item xs={12} mt={1}>
            <Grid container alignItems="center" justifyContent="center">
              <Grid item>
                <Stack alignItems="center" justifyContent="center" spacing={1}>
                  <Typography gutterBottom variant={'h5'}>
                    Đăng ký
                  </Typography>
                </Stack>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <RegisterForm />
          </Grid>
        </Grid>
      </DialogContent>
      <Verify />
    </Dialog>
  )
}

export default ModalRegister
