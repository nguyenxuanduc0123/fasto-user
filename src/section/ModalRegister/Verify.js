import {
  Avatar,
  Box,
  Button,
  Dialog,
  DialogContent,
  FormHelperText,
  Grid,
  Typography,
} from '@mui/material'
import { useFormik } from 'formik'
import React from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { activeAccount } from '../../features/auth/authAction'
import { selectAuthResponseActive } from '../../features/auth/authSlice'
import {
  closeAllModal,
  selectVerifyModal,
} from '../../features/modalLogin/modalLoginSlice'
import AppTextField from '../../ui-component/AppTextField'

const Verify = () => {
  const dispatch = useAppDispatch()
  const verifyModal = useAppSelector(selectVerifyModal)
  const response = useAppSelector(selectAuthResponseActive)

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: { code: '' },
    onSubmit: (values) => {
      dispatch(activeAccount(values))
    },
  })

  return (
    <Dialog
      open={verifyModal}
      onClose={() => dispatch(closeAllModal())}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item sx={{ mb: 0 }}>
            <Grid container alignItems="center" justifyContent="center">
              <Grid item>
                <Avatar
                  alt="Logo"
                  src="images/logo2.png"
                  sx={{
                    width: '80px',
                    height: '80px',
                  }}
                />
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12} mt={1}>
            <Typography textAlign="center">
              Chúng tôi vừa gửi mã xác thực vào email của bạn, vui lòng kiểm tra
              để tiếp tục
            </Typography>
          </Grid>

          <Grid item xs={12} mt={1}>
            <Box
              component="form"
              onSubmit={formik.handleSubmit}
              sx={{ p: 1 }}
              noValidate
            >
              <AppTextField
                formik={formik}
                name="code"
                label="Mã xác thực"
                required
              />

              {response.error && (
                <FormHelperText error>{response.error}</FormHelperText>
              )}

              <Button
                disabled={!formik.values.code}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                sx={{ marginTop: '10px' }}
              >
                Xác thực
              </Button>
            </Box>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  )
}

export default Verify
