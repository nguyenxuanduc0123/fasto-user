import DateRangeOutlinedIcon from '@mui/icons-material/DateRangeOutlined'
import { LoadingButton } from '@mui/lab'
import { Box, Grid, IconButton, InputAdornment, TextField } from '@mui/material'
import { MobileDatePicker } from '@mui/x-date-pickers'
import { useFormik } from 'formik'
import React, { useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { register, selectAuthResponse } from '../../features/auth/authSlice'
import { openVerifyModal } from '../../features/modalLogin/modalLoginSlice'
import AppTextField from '../../ui-component/AppTextField'
import { registerSchema } from '../../validate/register'

const initValue = {
  birthDay: new Date().getTime(),
  email: '',
  firstname: '',
  lastname: '',
  password: '',
  phoneNumber: '',
}

const RegisterForm = () => {
  const dispatch = useAppDispatch()
  const [showSelectDateFrom, setShowSelectDateFrom] = useState(false)

  const response = useAppSelector(selectAuthResponse)

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: initValue,
    validationSchema: registerSchema,
    onSubmit: (values) => {
      try {
        dispatch(register(values))
        dispatch(openVerifyModal())
      } catch (err) {
        console.error(err)
      }
    },
  })

  return (
    <Box
      component="form"
      onSubmit={formik.handleSubmit}
      sx={{ p: 1 }}
      noValidate
    >
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <AppTextField formik={formik} name="email" label="Email" required />
        </Grid>

        <Grid item xs={12}>
          <AppTextField
            formik={formik}
            name="password"
            label="Password"
            required
            type="password"
          />
        </Grid>

        <Grid item xs={12}>
          <AppTextField
            formik={formik}
            name="firstname"
            label="First Name"
            required
          />
        </Grid>

        <Grid item xs={12}>
          <AppTextField
            formik={formik}
            name="lastname"
            label="Last Name"
            required
          />
        </Grid>

        <Grid item xs={12}>
          <AppTextField
            formik={formik}
            name="phoneNumber"
            label="Phone Number"
            required
          />
        </Grid>

        <Grid item xs={12}>
          <MobileDatePicker
            ampm={false}
            label="Birthday"
            inputFormat="yyyy/MM/dd"
            value={formik.values.birthDay}
            onChange={(newValue) => {
              formik.setFieldValue('birthDay', newValue.getTime() / 1000)
            }}
            open={showSelectDateFrom}
            onOpen={() => setShowSelectDateFrom(true)}
            onClose={() => setShowSelectDateFrom(false)}
            showToolbar={false}
            InputProps={{
              endAdornment: (
                <InputAdornment
                  position="end"
                  onClick={() => setShowSelectDateFrom(true)}
                >
                  <IconButton edge="end" size="large" sx={{ mr: '5px' }}>
                    <DateRangeOutlinedIcon />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            renderInput={(params) => (
              <TextField required fullWidth name="birthDay" {...params} />
            )}
          />
        </Grid>

        <Grid item xs={12}>
          <LoadingButton
            disableElevation
            disabled={response.status === 'pending'}
            fullWidth
            size="large"
            type="submit"
            variant="contained"
          >
            Đăng ký
          </LoadingButton>
        </Grid>
      </Grid>
    </Box>
  )
}

export default RegisterForm
