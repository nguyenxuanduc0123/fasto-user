import { Box, Typography } from '@mui/material'
import React from 'react'

const PaymentType = () => {
  return (
    <Box>
      <Typography variant="h6" fontWeight={600} mb={1}>
        Phương thức thanh toán
      </Typography>
      <Box display="flex" alignItems="center" mb={1}>
        <Box
          width="80px"
          height="80px"
          mr={1}
          sx={{ border: '1px solid red', borderRadius: 4, cursor: 'pointer' }}
        >
          <img
            src={'/images/vnpay.png'}
            alt=""
            className="object-cover w-[100%] h-[100%]  "
          />
        </Box>
      </Box>
    </Box>
  )
}

export default PaymentType
