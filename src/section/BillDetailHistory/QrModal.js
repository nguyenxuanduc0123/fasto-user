import DownloadIcon from '@mui/icons-material/Download'
import { Dialog, DialogContent, Grid } from '@mui/material'
import { QRCodeCanvas } from 'qrcode.react'
import React from 'react'
import { useAppDispatch } from '../../app/hooks'
import { closeAllModal } from '../../features/modalLogin/modalLoginSlice'

const QrModal = ({ qrModal, detailBill }) => {
  const dispatch = useAppDispatch()

  const downloadQR = () => {
    const canvas = document.getElementById('qrcode')
    const pngUrl = canvas
      .toDataURL('image/png')
      .replace('image/png', 'image/octet-stream')
    let downloadLink = document.createElement('a')
    downloadLink.href = pngUrl
    downloadLink.download = 'qr-code.png'
    document.body.appendChild(downloadLink)
    downloadLink.click()
    document.body.removeChild(downloadLink)
  }

  return (
    <Dialog
      open={qrModal}
      onClose={() => dispatch(closeAllModal())}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item>
            <QRCodeCanvas
              size="100"
              id="qrcode"
              value={detailBill?.code}
              imageSettings={{
                src: '/images/logo2.png',
                excavate: true,
              }}
            />
            <DownloadIcon
              sx={{
                margin: 'auto',
                display: 'flex',
                fontSize: '32px',
                cursor: 'pointer',
                mt: 1,
              }}
              onClick={() => downloadQR()}
            />
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  )
}

export default QrModal
