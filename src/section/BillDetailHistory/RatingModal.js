import {
  Avatar,
  Box,
  Dialog,
  DialogContent,
  Grid,
  Rating,
  Stack,
  Typography,
} from '@mui/material'
import { useFormik } from 'formik'
import React from 'react'
import RatingAPI from '../../api/rating/api'
import { useAppDispatch } from '../../app/hooks'
import { closeAllModal } from '../../features/modalLogin/modalLoginSlice'
import AppTextField from '../../ui-component/AppTextField'
import MainButton from '../../ui-component/Button/MainButton'

const RatingModal = ({ ratingModal, detailBill }) => {
  const dispatch = useAppDispatch()

  const initValue = {
    billId: detailBill?.billId,
    shopId: detailBill?.shopId,
    content: '',
    start: 5,
  }

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: initValue,
    onSubmit: (values) => {
      try {
        RatingAPI.createRating(values)
      } catch (err) {
        console.error(err)
      }
    },
  })

  return (
    <Dialog
      open={ratingModal}
      onClose={() => dispatch(closeAllModal())}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <Grid container alignItems="center" justifyContent="center">
          <Grid item>
            <Avatar
              alt="Logo"
              src={detailBill?.logo}
              sx={{
                width: '80px',
                height: '80px',
              }}
            />
          </Grid>

          <Grid item xs={12} mt={1}>
            <Grid container alignItems="center" justifyContent="center">
              <Grid item>
                <Stack alignItems="center" justifyContent="center" spacing={1}>
                  <Typography gutterBottom textAlign="center" variant={'h5'}>
                    Gửi đánh giá cho chúng tôi
                  </Typography>
                </Stack>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Box component="form" onSubmit={formik.handleSubmit} noValidate>
              <Grid container spacing={1} px={2}>
                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                  <Rating
                    name="simple-controlled"
                    value={formik.values.start}
                    onChange={(event, newValue) => {
                      formik.setFieldValue('start', newValue)
                    }}
                  />
                </Grid>

                <Grid item xs={12}>
                  <AppTextField
                    formik={formik}
                    name="content"
                    label="Nội dung"
                    required
                  />
                </Grid>
              </Grid>

              <Grid item xs={12} mt={2} display="flex" justifyContent="center">
                <MainButton
                  onClick={() => {
                    formik.handleSubmit()
                    dispatch(closeAllModal())
                  }}
                >
                  Gửi đánh giá
                </MainButton>
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  )
}

export default RatingModal
