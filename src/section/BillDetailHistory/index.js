import FavoriteIcon from '@mui/icons-material/Favorite'
import LocationOnIcon from '@mui/icons-material/LocationOn'
import { Box, Button, Divider, Stack, Typography } from '@mui/material'
import { QRCodeCanvas } from 'qrcode.react'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import BillAPI from '../../api/bill/api'
import { colors } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  openQRModal,
  openRatingModal,
  selectQRModal,
  selectRatingModal,
} from '../../features/modalLogin/modalLoginSlice'
import MainCard from '../../ui-component/MainCard'
import { formatPrice } from '../../util/helpers'
import QrModal from './QrModal'
import RatingModal from './RatingModal'

const BillDetailHistory = () => {
  const dispatch = useAppDispatch()
  const qrModal = useAppSelector(selectQRModal)
  const ratingModal = useAppSelector(selectRatingModal)

  const [detailBill, setDetailBill] = useState({})

  const { billId } = useParams()

  const fetchDetailBill = async () => {
    const data = await BillAPI.getDetailBill({ id: billId })
    setDetailBill(data.data)
  }

  useEffect(() => {
    if (billId) {
      fetchDetailBill()
    }
  }, [billId])

  return (
    <MainCard>
      <Box sx={{ width: '100%' }}>
        {detailBill?.status === 'PAYMENTED' && (
          <Box display="flex" justifyContent="center" mb={1} mt={2}>
            <QRCodeCanvas
              size="92"
              id="qrcode"
              value={detailBill?.code}
              imageSettings={{
                src: '/images/logo2.png',
                excavate: true,
              }}
              onClick={() => dispatch(openQRModal(true))}
            />
          </Box>
        )}

        <Typography textAlign="center">
          <span className="font-semibold">{detailBill?.shopName}</span> biết bạn
          có nhiều sự lựa chọn, cảm ơn vì đã chọn{' '}
          <span className="font-semibold">{detailBill?.shopName}</span> ngày hôm
          nay.️️ <FavoriteIcon sx={{ color: 'red' }} />
        </Typography>

        <Divider sx={{ my: 2 }} />

        <Box display="flex">
          <Box>
            <LocationOnIcon
              sx={{
                color: colors.mainColor,
              }}
            />
          </Box>
          <Box ml={0.5}>
            <Typography fontWeight={600}>{detailBill?.shopName}</Typography>
            <Typography
              mb={1}
              onClick={() =>
                window.open(
                  `https://maps.google.com?q=${detailBill?.x},${detailBill?.y}`,
                  '_blank'
                )
              }
              sx={{ cursor: 'pointer' }}
            >{`${detailBill?.street || ''}, ${detailBill?.district}, ${
              detailBill?.city
            }`}</Typography>

            {detailBill?.status === 'DONE' && (
              <Button
                variant="contained"
                onClick={() => dispatch(openRatingModal(true))}
              >
                Đánh giá
              </Button>
            )}
          </Box>
        </Box>

        <Divider sx={{ my: 2 }} />

        <Box>
          <Typography variant="h6" fontWeight={600} mb={1}>
            Chi tiết đơn hàng
          </Typography>

          {detailBill?.billItemResponseDtos?.map((item, index) => (
            <Box
              key={index}
              mt={2}
              maxHeight="65vh"
              sx={{
                overflow: 'hidden',
                overflowY: 'auto',
              }}
            >
              <Box display="flex" alignItems="center" mb={1}>
                <Typography ml={2}>{item?.amount} x</Typography>
                <Box width="15%" mx={1}>
                  <img
                    src={item?.image}
                    alt=""
                    className="object-cover h-[80px] rounded"
                  />
                </Box>
                <Typography
                  variant="h6"
                  sx={{
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    display: '-webkit-box',
                    WebkitLineClamp: '2',
                    WebkitBoxOrient: 'vertical',
                  }}
                >
                  {item?.nameProduct}
                </Typography>

                <Box
                  display="flex"
                  alignItems="center"
                  sx={{ marginLeft: 'auto' }}
                >
                  <Typography variant="h6">{`${formatPrice(
                    item?.price * item?.amount || 0
                  )} đ`}</Typography>
                </Box>
              </Box>
              <Divider sx={{ my: 2 }} />
            </Box>
          ))}

          <Typography variant="h6" fontWeight={600} mb={1}>
            Voucher
          </Typography>

          <Box ml={2}>
            <Stack direction="row" justifyContent="space-between">
              <Typography variant="body1" fontWeight={600}>
                Mã giảm giá:
              </Typography>
              <Typography variant="body1" fontWeight={600}>
                {detailBill?.voucherName}
              </Typography>
            </Stack>

            <Stack direction="row" justifyContent="space-between">
              <Typography variant="body1" fontWeight={600}>
                Tổng (tạm tính):
              </Typography>
              <Typography variant="body1" fontWeight={600}>
                {formatPrice(detailBill?.totalOrigin || 0)} đ
              </Typography>
            </Stack>

            <Stack direction="row" justifyContent="space-between">
              <Typography variant="body1" fontWeight={600}>
                Giảm giá:
              </Typography>
              <Typography variant="body1" fontWeight={600} color="red">
                -{' '}
                {detailBill?.voucherId
                  ? formatPrice(detailBill?.totalDiscount)
                  : formatPrice(0)}{' '}
                đ
              </Typography>
            </Stack>

            <Divider sx={{ my: 2 }} />

            <Stack direction="row" justifyContent="space-between">
              <Typography variant="body1" fontWeight={600}>
                Tổng thanh toán:
              </Typography>
              <Typography variant="body1" fontWeight={600}>
                {formatPrice(detailBill?.totalPayment || 0)} đ
              </Typography>
            </Stack>
          </Box>
        </Box>
      </Box>

      <QrModal qrModal={qrModal} detailBill={detailBill} />
      <RatingModal ratingModal={ratingModal} detailBill={detailBill} />
    </MainCard>
  )
}

export default BillDetailHistory
