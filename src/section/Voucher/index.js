import { Box, Card, CardMedia, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Slider from 'react-slick'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  getListVoucher,
  selectListVoucher,
  selectListVoucherQuery,
} from '../../features/voucher/voucherSlice'
import VoucherDialog from './VoucherDialog'

const Voucher = () => {
  const navigate = useNavigate()
  const [selectedVoucher, setSelectedVoucher] = useState(undefined)

  const dispatch = useAppDispatch()

  const voucherList = useAppSelector(selectListVoucher)
  const query = useAppSelector(selectListVoucherQuery)

  useEffect(() => {
    fetchDataList({ ...query, page: 0 })
  }, [dispatch])

  const fetchDataList = ({ page = 0, size = 10, query = '' }) => {
    dispatch(getListVoucher({ page, size, query }))
  }

  const settings = {
    dots: true,
    autoplay: true,
    autoplaySpeed: 2000,
    infinite: true,
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 3,
    centerMode: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  }

  return (
    <Box sx={{ padding: { xs: 2, sm: 4 } }}>
      <Typography variant="h5" fontWeight={600} mb={1}>
        Voucher
      </Typography>
      <Slider {...settings}>
        {voucherList?.content?.map((item, index) => (
          <Card
            key={index}
            sx={{
              maxWidth: 440,
            }}
            onClick={() => setSelectedVoucher(item.id)}
          >
            <CardMedia
              sx={{
                height: 270,
                cursor: 'pointer',
              }}
              image={item.image}
              title={item.name}
            />
          </Card>
        ))}
      </Slider>

      <VoucherDialog
        selectedVoucher={selectedVoucher}
        setSelectedVoucher={setSelectedVoucher}
      />
    </Box>
  )
}

export default Voucher
