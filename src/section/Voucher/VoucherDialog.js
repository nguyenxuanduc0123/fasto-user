import {
  Dialog,
  DialogActions,
  DialogContent,
  Grid,
  Stack,
  Typography,
} from '@mui/material'
import { format } from 'date-fns'
import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  getDetailVoucher,
  selectDetailVoucher,
} from '../../features/voucher/voucherSlice'
import MainButton from '../../ui-component/Button/MainButton'
import { formatPrice } from '../../util/helpers'

const VoucherDialog = ({ selectedVoucher, setSelectedVoucher }) => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const detailVoucher = useAppSelector(selectDetailVoucher)

  useEffect(() => {
    if (selectedVoucher) {
      fetchDataList()
    }
  }, [selectedVoucher, dispatch])

  const fetchDataList = () => {
    dispatch(getDetailVoucher({ id: selectedVoucher }))
  }

  return (
    <Dialog
      open={selectedVoucher}
      onClose={() => setSelectedVoucher(undefined)}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item xs={12} mt={1}>
            <Stack alignItems="center" justifyContent="center" spacing={1}>
              <Typography gutterBottom textAlign="center" variant={'h5'}>
                {detailVoucher?.name}
              </Typography>
              <img src={detailVoucher?.image} alt="" />
              <Typography>
                {detailVoucher?.voucherType === 'PRICE'
                  ? `Giảm giá ${formatPrice(
                      detailVoucher?.valueDiscount || 0
                    )}đ cho đơn hàng từ ${
                      formatPrice(detailVoucher?.valueNeed) || 0
                    }đ`
                  : `Giảm giá ${
                      detailVoucher?.valueDiscount
                    }%, tối đa ${formatPrice(
                      detailVoucher?.maxDiscount || 0
                    )} cho đơn hàng từ ${formatPrice(
                      detailVoucher?.valueNeed || 0
                    )}đ`}
              </Typography>
              <Typography>
                {`Hạn sử dụng: ${format(
                  new Date(detailVoucher?.endedAt).getTime() ||
                    new Date().getTime(),
                  'dd-MM-yyyy'
                )}`}
              </Typography>
            </Stack>
          </Grid>
        </Grid>
      </DialogContent>

      <DialogActions>
        <MainButton onClick={() => navigate(`/store/${detailVoucher?.shopId}`)}>
          Sử dụng
        </MainButton>
      </DialogActions>
    </Dialog>
  )
}

export default VoucherDialog
