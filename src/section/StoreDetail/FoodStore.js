import AddIcon from '@mui/icons-material/Add'
import LocalOfferOutlinedIcon from '@mui/icons-material/LocalOfferOutlined'
import { Box, Grid, Typography } from '@mui/material'
import React, { useState } from 'react'
import { colors } from '../../app/constant'
import AppIconButton from '../../ui-component/Button/AppIconButton'
import { formatPrice } from '../../util/helpers'
import FoodModal from './FoodModal'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { selectFoodModal } from '../../features/modalLogin/modalLoginSlice'
import { openFoodModal } from '../../features/modalLogin/modalLoginSlice'
import { selectListCart } from '../../features/cart/cartSlice'

const FoodStore = ({ listProduct, storeId }) => {
  const [detailFood, setDetailFood] = useState({})
  const [inCart, setInCart] = useState(false)

  const dispatch = useAppDispatch()
  const foodModal = useAppSelector(selectFoodModal)

  const listCart = useAppSelector(selectListCart)

  const checkInCart = (item) => {
    let temp
    if (Object.keys(listCart).length > 0) {
      for (const [key] of Object.entries(listCart)) {
        listCart[key].forEach((values) => {
          if (values.id === item.id) {
            temp = values
          }
        })
      }
    }
    return temp
  }

  return (
    <Grid container sx={{ padding: { xs: 2, sm: 4 } }}>
      <Grid item xs={12}>
        <Typography variant="h5" fontWeight={600} mb={1}>
          Menu
        </Typography>
      </Grid>

      <Grid item xs={12}>
        <Grid container spacing={2}>
          {listProduct?.map((item, index) => (
            <Grid
              key={index}
              item
              xs={12}
              sm={6}
              md={4}
              sx={{
                display: 'flex',
                flexDirection: 'column',
              }}
              onClick={() => {
                setDetailFood(item)
                dispatch(openFoodModal())
                setInCart(!!checkInCart(item))
              }}
            >
              <Box
                display="flex"
                flex={1}
                sx={{
                  backgroundColor: colors.whiteColor,
                  borderRadius: '6px',
                  border: '1px solid transparent',
                  borderRight:
                    checkInCart(item) && `5px solid ${colors.mainColor}`,
                  cursor: 'pointer',
                  '&:hover': {
                    border: `1px solid ${colors.mainColor}`,
                    borderRight:
                      checkInCart(item) && `5px solid ${colors.mainColor}`,
                  },
                  padding: { xs: 0, sm: 2 },
                }}
              >
                <Box sx={{ width: '45%' }}>
                  <img
                    src={item.image}
                    alt={item.name}
                    className="h-[160px] object-cover"
                  />
                </Box>

                <Box flex={1} ml={2} display="flex" flexDirection="column">
                  {checkInCart(item) && (
                    <Typography
                      sx={{ color: colors.mainColor, fontWeight: 600 }}
                    >
                      {`${checkInCart(item).amount}x`}
                    </Typography>
                  )}
                  <Typography variant="h6">
                    {item.name}{' '}
                    {checkInCart(item) && (
                      <LocalOfferOutlinedIcon
                        sx={{ color: colors.mainColor }}
                      />
                    )}
                  </Typography>
                  <Typography variant="p">{item.description}</Typography>
                  <Box
                    display="flex"
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ marginTop: 'auto' }}
                  >
                    <Typography variant="h6" sx={{ color: '#0288d1' }}>
                      {formatPrice(item.price)} đ
                    </Typography>
                    <AppIconButton>
                      <AddIcon sx={{ color: colors.whiteColor }} />
                    </AppIconButton>
                  </Box>
                </Box>
              </Box>
            </Grid>
          ))}
        </Grid>
      </Grid>

      <FoodModal
        open={foodModal}
        detailFood={detailFood}
        storeId={storeId}
        inCart={inCart}
      />
    </Grid>
  )
}

export default FoodStore
