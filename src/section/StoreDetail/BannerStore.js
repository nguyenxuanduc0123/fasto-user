import AccessTimeIcon from '@mui/icons-material/AccessTime'
import DescriptionIcon from '@mui/icons-material/Description'
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord'
import LocationOnIcon from '@mui/icons-material/LocationOn'
import {
  Avatar,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  Rating,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import { colors } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  closeAllModal,
  openFeedbackModal,
  selectFeedbackModal,
} from '../../features/modalLogin/modalLoginSlice'
import MainCard from '../../ui-component/MainCard'
import RatingAPI from '../../api/rating/api'
import { useParams } from 'react-router-dom'
import EmptyData from '../../ui-component/EmptyData'

const BannerStore = ({ shopProfile }) => {
  const dispatch = useAppDispatch()
  const { storeId } = useParams()

  const feedbackModal = useAppSelector(selectFeedbackModal)

  const [feedbackList, setFeedbackList] = useState([])

  const fetchFeedback = async () => {
    const data = await RatingAPI.getRating({ id: storeId })
    setFeedbackList(data.data.content)
  }

  useEffect(() => {
    if (storeId) {
      fetchFeedback()
    }
  }, [storeId])

  return (
    shopProfile && (
      <Grid
        container
        columnSpacing={2}
        sx={{ backgroundColor: '#fff', padding: { xs: 2, sm: 4 } }}
      >
        <Grid item xs={12} sm={5}>
          <img
            src={shopProfile?.banner}
            alt={shopProfile?.name}
            className="w-[100%] max-h-[50vh] object-cover"
          />
        </Grid>

        <Grid
          item
          xs={12}
          sm={7}
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
          }}
        >
          <Typography variant="h4">{shopProfile?.name}</Typography>
          <Typography variant="p">
            {`${shopProfile?.streetAddress || ''}, ${
              shopProfile?.stateProvince
            }, ${shopProfile?.city}`}
          </Typography>

          <Box display="flex" alignItems="center" my={1}>
            <Rating
              name="half-rating-read"
              defaultValue={5}
              precision={0.5}
              readOnly
              value={shopProfile?.startRatings || 5}
            />
            <Typography
              sx={{
                backgroundColor: colors.mainColor,
                borderRadius: 2,
                color: colors.whiteColor,
              }}
              mx={1}
              p={0.5}
            >
              {shopProfile?.ranting || 0}
            </Typography>
            <Typography>
              Đánh giá{' '}
              <span
                onClick={() => dispatch(openFeedbackModal(true))}
                className="font-bold text-[#ee4d2d] cursor-pointer"
              >
                (Xem tất cả)
              </span>
            </Typography>
          </Box>

          <Box display="flex" alignItems="center" mb={1}>
            <Box display="flex" alignItems="center" sx={{ color: 'green' }}>
              <FiberManualRecordIcon />
              <Typography>Mở cửa</Typography>
            </Box>
            <Box display="flex" alignItems="center" pl={2}>
              <AccessTimeIcon />
              <Typography ml={0.5}>8:00 - 22:00</Typography>
            </Box>
          </Box>

          <Box display="flex">
            <DescriptionIcon />
            <Typography textAlign="justify">
              {shopProfile?.description}
            </Typography>
          </Box>

          <Divider sx={{ my: 2 }} />

          <Box display="flex" justifyContent="space-between">
            <Box
              display="flex"
              alignItems="center"
              onClick={() =>
                window.open(
                  `https://maps.google.com?q=${shopProfile.x},${shopProfile.y}`,
                  '_blank'
                )
              }
              sx={{ cursor: 'pointer' }}
            >
              <LocationOnIcon
                sx={{
                  color: colors.mainColor,
                }}
              />
              <Typography ml={0.5}>Xem bản đồ</Typography>
            </Box>
            <Box>
              <Typography variant="body2">Dịch vụ bởi</Typography>
              <Typography variant="h6" sx={{ color: colors.mainColor }}>
                FastO
              </Typography>
            </Box>
          </Box>

          <Dialog
            open={feedbackModal}
            onClose={() => dispatch(closeAllModal())}
            aria-labelledby={'Bill Detail'}
            aria-describedby={'Bill Detail'}
            maxWidth="sm"
            fullWidth
          >
            <DialogTitle>Đánh giá cửa hàng</DialogTitle>

            <DialogContent>
              <MainCard mt={1}>
                <Stack textAlign="center">
                  <Typography fontWeight={600} variant="h5" my={1}>
                    {shopProfile?.name}
                  </Typography>
                  <Typography fontWeight={600}>{`${
                    shopProfile?.streetAddress || ''
                  }, ${shopProfile?.stateProvince}, ${
                    shopProfile?.city
                  }`}</Typography>
                  <Stack
                    direction="row"
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    my={1}
                  >
                    <Rating
                      name="half-rating-read"
                      precision={0.5}
                      readOnly
                      value={shopProfile?.startRatings || 5}
                    />
                    <Typography
                      sx={{
                        backgroundColor: colors.mainColor,
                        borderRadius: 2,
                        color: colors.whiteColor,
                      }}
                      mx={1}
                      p={0.5}
                    >
                      {shopProfile?.ranting || 0}
                    </Typography>
                    <Typography>Đánh giá</Typography>
                  </Stack>
                </Stack>
              </MainCard>

              {feedbackList?.length > 0 ? (
                feedbackList?.map((item, index) => (
                  <MainCard mt={1} key={index}>
                    <Box display="flex">
                      <Box>
                        <Avatar src={item?.userImage} />
                      </Box>
                      <Box ml={0.5}>
                        <Typography fontWeight={600}>
                          {item?.userFirstName}
                        </Typography>
                        <Rating
                          name="half-rating-read"
                          defaultValue={item?.ratings}
                          precision={0.5}
                          readOnly
                          value={item.ratings}
                        />
                        {item?.content && (
                          <Typography>{item?.content}</Typography>
                        )}
                      </Box>
                    </Box>
                  </MainCard>
                ))
              ) : (
                <MainCard mt={1}>
                  <EmptyData title="Chưa có đánh giá" />
                </MainCard>
              )}
            </DialogContent>

            <DialogActions>
              <Button onClick={() => dispatch(closeAllModal())}>Đóng</Button>
            </DialogActions>
          </Dialog>
        </Grid>
      </Grid>
    )
  )
}

export default BannerStore
