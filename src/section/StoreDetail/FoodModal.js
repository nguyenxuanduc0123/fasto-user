import { Box, Dialog, DialogContent, Typography } from '@mui/material'
import React from 'react'
import { colors } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { addToCart } from '../../features/cart/cartSlice'
import {
  closeAllModal,
  openLoginModal,
} from '../../features/modalLogin/modalLoginSlice'
import MainButton from '../../ui-component/Button/MainButton'
import { formatPrice } from '../../util/helpers'
import { selectAccount } from '../../features/account/accountSlice'

const FoodModal = ({ open, detailFood, inCart, storeId }) => {
  const dispatch = useAppDispatch()

  const isLogin = useAppSelector(selectAccount)

  const handleAddToCart = (foodDetail, amount) => {
    if (!isLogin) {
      dispatch(openLoginModal())
      return
    }

    const cartData = {
      cartDtos: [
        {
          amount: amount,
          id: foodDetail.id,
          image: foodDetail.image,
          name: foodDetail.name,
          price: foodDetail.price,
          shopId: storeId,
        },
      ],
      shopId: storeId,
    }
    dispatch(addToCart(cartData))
  }

  return (
    <Dialog
      open={open}
      onClose={() => dispatch(closeAllModal())}
      maxWidth="sm"
      fullWidth
    >
      <DialogContent sx={{ padding: 2 }}>
        <Box
          display="flex"
          flex={1}
          sx={{
            backgroundColor: colors.whiteColor,
            borderRadius: '6px',
            border: '1px solid transparent',
            cursor: 'pointer',
          }}
        >
          <Box sx={{ width: '35%' }}>
            <img
              src={detailFood?.image}
              alt={detailFood?.name}
              className="h-[140px] object-cover"
            />
          </Box>

          <Box flex={1} ml={2} display="flex" flexDirection="column">
            <Typography variant="h6">{detailFood?.name}</Typography>
            <Typography variant="p">{detailFood?.description}</Typography>
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              sx={{ marginTop: 'auto' }}
            >
              <Typography variant="h6" sx={{ color: '#0288d1' }}>
                {formatPrice(detailFood?.price || 0)} đ
              </Typography>
              <Box>
                {inCart ? (
                  <MainButton
                    onClick={() => {
                      handleAddToCart(detailFood, 0)
                    }}
                  >
                    Xóa
                  </MainButton>
                ) : (
                  <MainButton
                    onClick={() => {
                      handleAddToCart(detailFood, 1)
                    }}
                  >
                    Thêm
                  </MainButton>
                )}
              </Box>
            </Box>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  )
}

export default FoodModal
