import ShoppingBagOutlinedIcon from '@mui/icons-material/ShoppingBagOutlined'
import {
  Badge,
  Box,
  Drawer,
  Typography,
  useMediaQuery,
  useTheme,
} from '@mui/material'
import React, { useState } from 'react'
import { colors } from '../../app/constant'
import MainButton from '../../ui-component/Button/MainButton'
import { calculatePrice, countItem, formatPrice } from '../../util/helpers'
import Cart from '../Cart'
const CartHeader = ({ cartInfo }) => {
  const theme = useTheme()

  const [anchorElNav, setAnchorElNav] = useState(null)
  const totalPrice = calculatePrice(cartInfo)

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget)
  }
  const handleCloseNavMenu = () => {
    setAnchorElNav(null)
  }

  const totalItem = countItem(cartInfo)

  const matches = useMediaQuery(theme.breakpoints.up('sm'))

  return (
    <>
      {matches && (
        <Badge
          invisible={totalItem < 0}
          badgeContent={totalItem}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
          sx={{
            '& .MuiBadge-badge': {
              color: colors.mainColor,
              backgroundColor: colors.whiteColor,
              border: `1px solid ${colors.mainColor}`,
              fontWeight: 600,
            },
          }}
        >
          <MainButton
            onClick={handleOpenNavMenu}
            sx={
              Object.keys(cartInfo).length < 0
                ? {
                    color: '#ccc',
                    '&:hover': {
                      color: '#ccc',
                    },
                    paddingY: 1,
                    marginRight: 2,
                  }
                : {
                    backgroundColor: colors.mainColor,
                    color: colors.whiteColor,
                    '&:hover': {
                      backgroundColor: colors.mainColor,
                      color: colors.whiteColor,
                    },
                    paddingY: 1,
                    marginRight: 2,
                  }
            }
          >
            <Typography sx={{ display: 'flex', alignItems: 'center' }}>
              <ShoppingBagOutlinedIcon sx={{ marginRight: 0.5 }} />{' '}
              {totalPrice > 0 ? `${formatPrice(totalPrice)} đ` : null}
            </Typography>
          </MainButton>
        </Badge>
      )}

      <Drawer
        anchor={matches ? 'right' : 'bottom'}
        open={anchorElNav}
        onClose={handleCloseNavMenu}
        sx={{
          '& .MuiPaper-root': {
            color: '#333',
            padding: '20px',
            width: { sm: '70vw', md: '45vw' },
          },
        }}
      >
        <Cart handleCloseNavMenu={handleCloseNavMenu} />
      </Drawer>

      {!matches && (
        <Box sx={{ position: 'absolute', bottom: '-85vh', right: 10 }}>
          <MainButton
            onClick={handleOpenNavMenu}
            sx={
              Object.keys(cartInfo).length < 0
                ? {
                    color: '#ccc',
                    '&:hover': {
                      color: '#ccc',
                    },
                    paddingY: 1,
                    marginRight: 2,
                  }
                : {
                    backgroundColor: colors.mainColor,
                    color: colors.whiteColor,
                    '&:hover': {
                      backgroundColor: colors.mainColor,
                      color: colors.whiteColor,
                    },
                    paddingY: 1,
                    marginRight: 2,
                  }
            }
          >
            <Typography sx={{ display: 'flex', alignItems: 'center' }}>
              <ShoppingBagOutlinedIcon sx={{ marginRight: 0.5 }} />{' '}
              {totalPrice > 0 ? `${formatPrice(totalPrice)} đ` : null}
            </Typography>
          </MainButton>
        </Box>
      )}
    </>
  )
}

export default CartHeader
