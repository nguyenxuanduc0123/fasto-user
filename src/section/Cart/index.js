import AddIcon from '@mui/icons-material/Add'
import CloseIcon from '@mui/icons-material/Close'
import RemoveIcon from '@mui/icons-material/Remove'
import {
  Box,
  Divider,
  IconButton,
  Radio,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import { colors } from '../../app/constant'
import { useAppSelector } from '../../app/hooks'
import { selectListCart } from '../../features/cart/cartSlice'
import { selectListStore } from '../../features/store/storeSlice'
import useChangeAmount from '../../hooks/useChangeAmount'
import AppIconButton from '../../ui-component/Button/AppIconButton'
import MainButton from '../../ui-component/Button/MainButton'
import { calculatePrice, formatPrice, totalOfCart } from '../../util/helpers'
import { useNavigate } from 'react-router-dom'
import EmptyData from '../../ui-component/EmptyData'

const Cart = ({ handleCloseNavMenu }) => {
  const navigate = useNavigate()
  const [cartData, setCartData] = useState([])
  const [keys, setKeys] = useState([])
  const [selectedValue, setSelectedValue] = useState()

  const listCart = useAppSelector(selectListCart)
  const listShop = useAppSelector(selectListStore)

  const { handleChangeAmount } = useChangeAmount()

  const convertData = () => {
    let test = []
    for (const [key] of Object.entries(listCart)) {
      const a = listShop.content.find((it) => it.id == key)
      if (a) {
        test.push({ [key]: listCart[key], shopName: a.name, shopId: a.id })
      }
    }
    setCartData(test)
  }

  const getKey = () => {
    const listKey = []
    for (const [key] of Object.entries(listCart)) {
      listKey.push(key)
    }
    setKeys(listKey)
  }

  useEffect(() => {
    convertData()
    getKey()
    if (selectedValue) {
      setSelectedValue(undefined)
    }
  }, [listCart])

  const handleClick = () => {
    navigate('/cart', { state: { data: selectedValue } })
  }

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
      <Box>
        <Typography textAlign="center" variant="h5">
          Giỏ hàng
        </Typography>
        <IconButton sx={{ position: 'absolute', top: 10, left: 10 }}>
          <CloseIcon onClick={() => handleCloseNavMenu()} />
        </IconButton>
      </Box>

      <Divider sx={{ my: 2 }} />

      <Box sx={{ overflow: 'hidden', overflowY: 'auto', height: '70vh' }}>
        {cartData?.length > 0 ? (
          cartData?.map((item) => {
            return (
              <>
                <Stack direction="row" alignItems="center">
                  <Radio
                    checked={selectedValue?.shopId === item?.shopId}
                    onChange={() => {
                      setSelectedValue(item)
                    }}
                    size="small"
                  />
                  <Typography variant="h5">{item.shopName}</Typography>
                </Stack>
                {keys?.map((key) => {
                  return item[key]?.map((it) => {
                    return (
                      <Box mt={2}>
                        <Box display="flex" alignItems="center" mb={1}>
                          <Box width="15%" mr={1}>
                            <img
                              src={it?.image}
                              alt=""
                              className="object-cover h-[80px] rounded"
                            />
                          </Box>
                          <Stack flex={1}>
                            <Typography
                              variant="h6"
                              sx={{
                                overflow: 'hidden',
                                textOverflow: 'ellipsis',
                                display: '-webkit-box',
                                WebkitLineClamp: '2',
                                WebkitBoxOrient: 'vertical',
                              }}
                            >
                              {it?.name}
                            </Typography>
                            <Typography variant="h6">{`${formatPrice(
                              it?.price * it?.amount
                            )} đ`}</Typography>
                          </Stack>

                          <Box display="flex" alignItems="center" mr={2}>
                            <AppIconButton
                              onClick={() => {
                                handleChangeAmount(it, -1)
                              }}
                            >
                              <RemoveIcon sx={{ color: colors.whiteColor }} />
                            </AppIconButton>
                            <Typography mx={2}>{it?.amount}</Typography>
                            <AppIconButton
                              onClick={() => {
                                handleChangeAmount(it, 1)
                              }}
                            >
                              <AddIcon sx={{ color: colors.whiteColor }} />
                            </AppIconButton>
                          </Box>
                        </Box>
                        <Divider sx={{ my: 2 }} />
                      </Box>
                    )
                  })
                })}
              </>
            )
          })
        ) : (
          <EmptyData title="Giỏ hàng trống" />
        )}
      </Box>

      <Box
        pt={2}
        borderTop="1px solid #ccc"
        sx={{
          backgroundColor: colors.whiteColor,
          marginTop: 'auto',
        }}
      >
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          mb={2}
        >
          <Typography variant="h6">Tổng cộng</Typography>
          <Typography variant="h6">{`${formatPrice(
            calculatePrice(listCart)
          )} đ`}</Typography>
        </Box>

        <Box width="100%" textAlign="center">
          <MainButton
            fullWidth
            disabled={!selectedValue}
            onClick={handleClick}
          >{`Thanh toán | ${formatPrice(
            selectedValue ? totalOfCart(selectedValue) : 0
          )} đ`}</MainButton>
        </Box>
      </Box>
    </Box>
  )
}

export default Cart
