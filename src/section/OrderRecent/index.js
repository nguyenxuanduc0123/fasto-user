import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import BillAPI from '../../api/bill/api'
import { colors } from '../../app/constant'

const OrderRecent = () => {
  const navigate = useNavigate()
  const [listBill, setListBill] = useState([])

  const fetchListBill = async ({ query = '' }) => {
    const data = await BillAPI.getListBill({ query, status: 'DONE' })
    setListBill(data.data)
  }

  useEffect(() => {
    fetchListBill({ query: '' })
  }, [])

  return (
    <Grid container sx={{ padding: { xs: 2, sm: 4 } }}>
      <Grid item xs={12}>
        <Stack>
          <Typography variant="h5" fontWeight={600}>
            Vừa đặt gần đây
          </Typography>
          <Box
            sx={{
              width: '100px',
              backgroundColor: colors.mainColor,
              height: '4px',
            }}
          ></Box>
        </Stack>
      </Grid>

      <Grid item xs={12} mt={2}>
        <Grid container spacing={2}>
          {listBill
            ?.reduce((cur, obj) => {
              if (!cur.length || obj.shopId !== cur[cur.length - 1].shopId) {
                cur.push(obj)
              }
              return cur
            }, [])
            .map((item, index) => (
              <Grid
                key={index}
                item
                xs={6}
                md={4}
                lg={3}
                sx={{
                  display: 'flex',
                }}
              >
                <Card
                  sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    flexDirection: 'column',
                    width: '100%',
                  }}
                  onClick={() => navigate(`store/${item.shopId}`)}
                >
                  <CardActionArea
                    sx={{ display: 'flex', flexDirection: 'column', flex: 1 }}
                  >
                    <CardMedia
                      sx={{
                        height: { xs: '10rem', sm: '15rem' },
                        width: '100%',
                      }}
                      image={item.logo}
                      title={item.name}
                    />
                    <CardContent
                      sx={{
                        width: '100%',
                        flex: 1,
                      }}
                    >
                      <Typography
                        variant="h5"
                        sx={{
                          overflow: 'hidden',
                          textOverflow: 'ellipsis',
                          display: '-webkit-box',
                          WebkitLineClamp: '2',
                          WebkitBoxOrient: 'vertical',
                        }}
                      >
                        {item.name}
                      </Typography>
                      <Typography variant="body2" color="text.secondary">
                        {`${item.street}, ${item.town}, ${item.city}`}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
        </Grid>
      </Grid>
    </Grid>
  )
}

export default OrderRecent
