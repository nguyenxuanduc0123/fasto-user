import { Box, Tab, Tabs } from '@mui/material'
import React, { useState } from 'react'
import MainCard from '../../ui-component/MainCard'
import OrderHistory from './OrderHistory'
import FavoriteShop from './FavoriteShop'

const PurchaseHistory = () => {
  const [currentTab, setCurrentTab] = useState(0)

  function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ padding: { sm: 3 } }}>
            <Box>{children}</Box>
          </Box>
        )}
      </div>
    )
  }

  const handleChange = (event, newValue) => {
    setCurrentTab(newValue)
  }

  return (
    <MainCard>
      <Box sx={{ width: '100%' }}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs
            value={currentTab}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            <Tab label="Lịch sử đơn hàng" />
            <Tab label="Cửa hàng yêu thích" />
          </Tabs>
        </Box>
        <TabPanel value={currentTab} index={0}>
          <OrderHistory />
        </TabPanel>
        <TabPanel value={currentTab} index={1}>
          <FavoriteShop />
        </TabPanel>
      </Box>
    </MainCard>
  )
}

export default PurchaseHistory
