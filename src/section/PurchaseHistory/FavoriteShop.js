import React, { useEffect, useState } from 'react'
import StoreAPI from '../../api/store/api'
import { Box, Divider, Stack, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom'

const FavoriteShop = () => {
  const navigate = useNavigate()
  const [favoriteList, setFavoriteList] = useState([])

  const fetchListFavorite = async () => {
    const res = await StoreAPI.getListFavoriteStore()
    setFavoriteList(res.data.content)
  }

  useEffect(() => {
    fetchListFavorite()
  }, [])

  return (
    <Box mt={2}>
      {favoriteList?.map((item, index) => (
        <Box
          key={index}
          sx={{ cursor: 'pointer' }}
          onClick={() => navigate(`/store/${item.id}`)}
        >
          <Box display="flex" alignItems="center" flexWrap="wrap">
            <Box
              sx={{ width: '140px', height: '100px', objectFit: 'contain' }}
              mr={2}
            >
              <img src={item.banner} />
            </Box>
            <Box flex={1}>
              <Stack direction="row">
                <Typography>Tên cửa hàng: </Typography>
                <Typography ml={0.5} fontWeight={600}>
                  {item.name}
                </Typography>
              </Stack>
              <Stack direction="row">
                <Typography>Số điện thoại: </Typography>
                <Typography ml={0.5} fontWeight={600}>
                  {item.phone}
                </Typography>
              </Stack>
              <Stack direction="row">
                <Typography>Trạng thái: </Typography>
                <Typography ml={0.5} fontWeight={600}>
                  {item.schedule}
                </Typography>
              </Stack>
            </Box>
          </Box>

          <Divider sx={{ my: 2 }} />
        </Box>
      ))}
    </Box>
  )
}

export default FavoriteShop
