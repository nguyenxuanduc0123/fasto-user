import {
  Box,
  Button,
  Divider,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import BillAPI from '../../api/bill/api'
import { statusBill } from '../../app/constant'
import { formatPrice } from '../../util/helpers'
import { format } from 'date-fns'

const OrderHistory = () => {
  const navigate = useNavigate()
  const [statusOfBill, setStatusOfBill] = useState('PENDING')
  const [listBill, setListBill] = useState([])

  const fetchListBill = async () => {
    const data = await BillAPI.getListBill({ status: statusOfBill })
    setListBill(data.data)
  }

  useEffect(() => {
    fetchListBill()
  }, [statusOfBill])
  return (
    <>
      <FormControl
        sx={{
          width: { xs: '100%', sm: '30%' },
          display: 'flex',
          marginLeft: 'auto',
          marginTop: { xs: 2, sm: 0 },
        }}
      >
        <InputLabel id="demo-simple-select-helper-label">Trạng thái</InputLabel>
        <Select
          value={statusOfBill}
          label={'Trạng thái'}
          onChange={(event) => {
            setStatusOfBill(event.target.value)
          }}
          defaultValue={'PENDING'}
        >
          {statusBill.map((item) => (
            <MenuItem key={item.value} value={item.value}>
              {item?.label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <Box mt={2}>
        {listBill?.map((item, index) => (
          <Box key={index}>
            <Box display="flex" alignItems="center" flexWrap="wrap">
              <Box sx={{ width: '90px' }} mr={2}>
                <img src={item.logo} />
              </Box>
              <Box sx={{ display: 'flex', flexWrap: 'wrap' }} flex={1}>
                <Box flex={1}>
                  <Stack direction="row">
                    <Typography>Mã đơn hàng: </Typography>
                    <Typography ml={0.5} fontWeight={600}>
                      {item.id}
                    </Typography>
                  </Stack>
                  <Stack direction="row">
                    <Typography>Tên cửa hàng: </Typography>
                    <Typography ml={0.5} fontWeight={600}>
                      {item.name}
                    </Typography>
                  </Stack>
                  <Stack direction="row">
                    <Typography>Tổng đơn hàng: </Typography>
                    <Typography ml={0.5} fontWeight={600}>
                      {`${formatPrice(item.totalPayment)}đ`}
                    </Typography>
                  </Stack>
                  <Stack direction="row">
                    <Typography>Ngày tạo đơn: </Typography>
                    <Typography ml={0.5} fontWeight={600}>{`${format(
                      new Date(item?.createdAt).getTime() || new Date(),
                      'dd-MM-yyyy'
                    )}`}</Typography>
                  </Stack>
                </Box>

                <Button
                  variant="contained"
                  onClick={() => navigate(`/bill/${item?.id}`)}
                  sx={{ height: 'fit-content', alignSelf: 'center' }}
                  ml={1}
                >
                  Xem chi tiết
                </Button>
              </Box>
            </Box>

            <Divider sx={{ my: 2 }} />
          </Box>
        ))}
      </Box>
    </>
  )
}

export default OrderHistory
