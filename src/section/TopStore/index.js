import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Grid,
  Rating,
  Stack,
  Typography,
} from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import StoreAPI from '../../api/store/api'
import { colors } from '../../app/constant'
import LocationOnIcon from '@mui/icons-material/LocationOn'

const TopStore = () => {
  const navigate = useNavigate()
  const [storeData, setStoreData] = useState({
    totalElements: 0,
    listStore: [],
  })
  const [page, setPage] = useState(0)

  const fetchStoreData = async ({ query = '', page = 0, size = 10 }) => {
    const data = await StoreAPI.getTopStore({ query, page, size })
    setStoreData({
      totalElements: data.data.totalElements,
      listStore: [...storeData.listStore, ...data.data.content],
    })
  }

  useEffect(() => {
    fetchStoreData({ query: '', page: page, size: 5 })
  }, [page])

  return (
    <Grid container sx={{ padding: { xs: 2, sm: 4 } }}>
      <Grid item xs={12}>
        <Stack>
          <Typography variant="h5" fontWeight={600}>
            Cửa hàng nổi bật
          </Typography>
          <Box
            sx={{
              width: '100px',
              backgroundColor: colors.mainColor,
              height: '4px',
            }}
          ></Box>
        </Stack>
      </Grid>

      <Grid item xs={12} mt={2}>
        <Grid container spacing={2}>
          {storeData?.listStore?.map((item, index) => (
            <Grid
              key={index}
              item
              xs={6}
              md={4}
              lg={3}
              sx={{
                display: 'flex',
              }}
            >
              <Card
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  flexDirection: 'column',
                  width: '100%',
                }}
                onClick={() => navigate(`store/${item.id}`)}
              >
                <CardActionArea
                  sx={{ display: 'flex', flexDirection: 'column', flex: 1 }}
                >
                  <CardMedia
                    sx={{ height: { xs: '10rem', sm: '15rem' }, width: '100%' }}
                    image={item.banner}
                    title={item.name}
                  />
                  <CardContent
                    sx={{
                      width: '100%',
                      flex: 1,
                    }}
                  >
                    <Typography
                      variant="h5"
                      sx={{
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        display: '-webkit-box',
                        WebkitLineClamp: '2',
                        WebkitBoxOrient: 'vertical',
                      }}
                    >
                      {item.name}
                    </Typography>
                    <Stack direction="row">
                      <LocationOnIcon
                        sx={{
                          color: colors.mainColor,
                          marginLeft: '-5px',
                        }}
                      />
                      <Typography variant="body2" color="text.secondary">
                        {`${item.streetAddress}, ${item.stateProvince}, ${item.city}`}
                      </Typography>
                    </Stack>
                    <Rating
                      name="half-rating-read"
                      defaultValue={item?.startRatings || 5}
                      precision={0.5}
                      readOnly
                      value={item.startRatings}
                    />
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          ))}
        </Grid>

        {storeData?.listStore?.length !== storeData?.totalElements && (
          <Grid container>
            <Grid
              item
              xs={12}
              sx={{
                display: 'flex',
                justifyContent: 'center',
              }}
            >
              <Box
                sx={{
                  backgroundColor: 'transparent',
                  cursor: 'pointer',
                }}
                onClick={() => setPage(page + 1)}
                mt={2}
              >
                <Stack sx={{ margin: 'auto', color: colors.mainColor }}>
                  <Typography>Xem Thêm</Typography>
                  <KeyboardArrowDownIcon sx={{ margin: 'auto' }} />
                </Stack>
              </Box>
            </Grid>
          </Grid>
        )}
      </Grid>
    </Grid>
  )
}

export default TopStore
