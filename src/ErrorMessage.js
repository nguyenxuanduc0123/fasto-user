import { FormHelperText } from '@mui/material'
import { ErrorMessage as FormikErrorMessage } from 'formik'

export default function ErrorMessage(props) {
  return (
    <FormikErrorMessage
      {...props}
      render={(msg) => <FormHelperText error>{msg}</FormHelperText>}
    />
  )
}
