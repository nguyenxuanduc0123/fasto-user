import { configureStore } from '@reduxjs/toolkit'
import { useMemo } from 'react'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './saga'
import { logger } from './middleware'
import storeReducer from '../features/store/storeSlice'
import voucherReducer from '../features/voucher/voucherSlice'
import accountReducer from '../features/account/accountSlice'
import cartReducer from '../features/cart/cartSlice'
import productReducer from '../features/product/productSlice'
import loginModalReducer from '../features/modalLogin/modalLoginSlice'
import authReducer from '../features/auth/authSlice'

export const saveState = (state) => {
  try {
    const seria = JSON.stringify(state)
    localStorage.setItem('state', seria)
  } catch (error) {
    //Ignore writting error
  }
}

let flag = true
let store

const initStore = (preloadedState) => {
  const sagaMiddleware = createSagaMiddleware()

  const store = configureStore({
    reducer: {
      store: storeReducer,
      modalLogin: loginModalReducer,
      auth: authReducer,
      cart: cartReducer,
      product: productReducer,
      voucher: voucherReducer,
      account: accountReducer,
    },
    preloadedState,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(logger, sagaMiddleware),
  })

  store.subscribe(() => {
    saveState({
      theme: store.getState().theme,
    })
  })

  sagaMiddleware.run(rootSaga)

  return store
}

export const configureAppStore = (preloadedState) => {
  let _store = store ?? initStore(preloadedState)

  if (preloadedState && flag) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    flag = false
  }

  if (typeof window === 'undefined') return _store
  if (!flag) {
    store = _store
    flag = true
  }

  return _store
}

export function useStore(initialState) {
  const store = useMemo(() => configureAppStore(initialState), [initialState])
  return store
}
