export const logger = (storeApi) => (next) => (action) => {
  // console.group(action.type)
  // console.info('dispatching', action)
  let result = next(action)
  // console.log('next state', storeApi.getState())
  // console.groupEnd()
  return result
}
