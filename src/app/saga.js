import { all } from 'redux-saga/effects'
import authSaga from '../features/auth/authSaga'
import { storeSaga } from '../features/store/storeSaga'
import { voucherSaga } from '../features/voucher/voucherSaga'
import { cartSaga } from '../features/cart/cartSaga'
import { productSaga } from '../features/product/productSaga'
import { accountSaga } from '../features/account/accountSaga'

export default function* rootSaga() {
  yield all([
    authSaga(),
    storeSaga(),
    cartSaga(),
    productSaga(),
    voucherSaga(),
    accountSaga(),
  ])
}
