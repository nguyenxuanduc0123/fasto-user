export const INIT_LIST = {
  result: [],
  paginate: {
    page: 1,
  },
}

export const INIT_REQUEST_STATUS = {
  processing: false,
  fail: false,
  success: false,
}

export const INIT_QUERY = {
  page: 1,
  size: 10,
}

export const colors = {
  mainColor: '#ee4d2d',
  primaryColor: '#3498db',
  whiteColor: '#fff',
  blueColor: '#2d32ee',
  greenColor: '#00ff31',
}

export const statusBill = [
  {
    label: 'PENDING',
    value: 'PENDING',
  },
  {
    label: 'FAILED',
    value: 'FAILED',
  },
  {
    label: 'DONE',
    value: 'DONE',
  },
  {
    label: 'PAYMENTED',
    value: 'PAYMENTED',
  },
  {
    label: 'CREATED',
    value: 'CREATED',
  },
]
