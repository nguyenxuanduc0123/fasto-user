import { useCallback } from 'react'
import { useAppDispatch } from '../app/hooks'
import { addToCart } from '../features/cart/cartSlice'

const useChangeAmount = () => {
  const dispatch = useAppDispatch()
  const handleChangeAmount = useCallback((foodDetail, number) => {
    const cartData = {
      cartDtos: [
        {
          amount: foodDetail.amount + number,
          id: foodDetail.id,
          image: foodDetail.image,
          name: foodDetail.name,
          price: foodDetail.price,
          shopId: foodDetail.shopId,
        },
      ],
      shopId: foodDetail.shopId,
    }
    dispatch(addToCart(cartData))
  }, [])

  return { handleChangeAmount }
}

export default useChangeAmount
