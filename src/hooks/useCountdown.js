import { useEffect, useState } from 'react'

const useCountdown = (second, onEnd) => {
  const [remaining, setRemaining] = useState(second)

  useEffect(() => {
    const tick = () => {
      setRemaining(remaining - 1)
    }

    const countdown = setInterval(tick, 1000)

    if (remaining < 0) {
      clearInterval(countdown)
      onEnd()
    }

    return () => clearInterval(countdown)
  }, [remaining])

  return remaining
}

export default useCountdown
