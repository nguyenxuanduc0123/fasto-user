import { SearchOffOutlined } from '@mui/icons-material'
import { Box, Typography } from '@mui/material'
import React from 'react'

const EmptyData = ({ title = 'Không có kết quả' }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Box sx={{ color: '#999' }}>
        <SearchOffOutlined sx={{ width: '80px', height: '80px' }} />
      </Box>
      <Typography variant="h5" sx={{ color: '#999', textAlign: 'center' }}>
        {title}
      </Typography>
    </Box>
  )
}

export default EmptyData
