import { Box } from '@mui/material'
import React from 'react'

const MainCard = ({ children, ...others }) => {
  return (
    <Box
      sx={{
        backgroundColor: '#fff',
        boxShadow: '10px 30px 80px rgba(0,0,0,0.3)',
        borderRadius: 2,
      }}
      p={2}
      {...others}
    >
      {children}
    </Box>
  )
}

export default MainCard
