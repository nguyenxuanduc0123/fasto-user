import { Button } from '@mui/material'
import React from 'react'
import { colors } from '../../app/constant'

const MainButton = (props) => {
  const children = props.children
  return (
    <Button
      variant="raised"
      sx={{
        backgroundColor: props.disabled ? '#ccc' : colors.mainColor,
        color: props.disabled ? '#ccc' : colors.whiteColor,
        '&:hover': {
          backgroundColor: colors.mainColor,
          color: colors.whiteColor,
        },
        paddingY: 1,
      }}
      {...props}
    >
      {children}
    </Button>
  )
}

export default MainButton
