import { IconButton } from '@mui/material'
import React from 'react'
import { colors } from '../../app/constant'

const AppIconButton = (props) => {
  const children = props.children
  return (
    <IconButton
      sx={{
        borderRadius: '6px',
        backgroundColor: colors.mainColor,
        width: '30px',
        height: '30px',
        '&:hover': {
          backgroundColor: colors.mainColor,
          color: colors.whiteColor,
        },
      }}
      {...props}
    >
      {children}
    </IconButton>
  )
}

export default AppIconButton
