export const formatPrice = (price) => {
  return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

export const totalOfCart = (data) => {
  const total = data[data.shopId].reduce((acc, item) => {
    return (acc += item.price * item.amount)
  }, 0)
  return total
}

export const calculatePrice = (cartData) => {
  let price = 0
  if (Object.keys(cartData).length > 0) {
    for (const [key] of Object.entries(cartData)) {
      cartData[key].forEach((item) => {
        price += item.price * item.amount
      })
    }
  }
  return price
}

export const countItem = (cartData) => {
  if (Object.keys(cartData).length > 0) {
    let amountItem = 0
    for (const [key] of Object.entries(cartData)) {
      amountItem += cartData[key].length
    }
    return amountItem
  }
}
