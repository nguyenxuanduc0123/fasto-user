import MainLayout from '../layout/MainLayout'
import Voucher from '../section/Voucher'
import Food from '../section/Food'
import StoreList from '../section/StoreList'
import OrderRecent from '../section/OrderRecent'
import BestSeller from '../section/BestSeller'
import TopStore from '../section/TopStore'
import ShopNearby from '../section/ShopNearby'
import { useAppSelector } from '../app/hooks'
import { selectAccount } from '../features/account/accountSlice'

function Home() {
  const isLogin = useAppSelector(selectAccount)

  return (
    <MainLayout>
      <div className="">
        <Voucher />
        <StoreList />
        {isLogin && <OrderRecent />}
        <BestSeller />
        <TopStore />
        <Food />
        <ShopNearby />
      </div>
    </MainLayout>
  )
}

export default Home
