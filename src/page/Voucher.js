import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../app/hooks'
import {
  getDetailVoucher,
  selectDetailVoucher,
} from '../features/voucher/voucherSlice'
import MainLayout from '../layout/MainLayout'

const Shop = () => {
  const dispatch = useAppDispatch()
  const { voucherId } = useParams()
  const detailVoucher = useAppSelector(selectDetailVoucher)

  useEffect(() => {
    if (voucherId) {
      fetchDataList()
    }
  }, [voucherId, dispatch])

  const fetchDataList = () => {
    dispatch(getDetailVoucher({ id: voucherId }))
  }
  return (
    <MainLayout>
      <div>{detailVoucher?.name}</div>
    </MainLayout>
  )
}

export default Shop
