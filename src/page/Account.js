import { Box, Grid } from '@mui/material'
import React from 'react'
import MainLayout from '../layout/MainLayout'
import AccountInfo from '../section/AccountInfo'
import PurchaseHistory from '../section/PurchaseHistory'

const Account = () => {
  return (
    <MainLayout>
      <Box sx={{ padding: { xs: 2, sm: 4 } }}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={3}>
            <AccountInfo />
          </Grid>

          <Grid item xs={12} md={9}>
            <PurchaseHistory />
          </Grid>
        </Grid>
      </Box>
    </MainLayout>
  )
}

export default Account
