import React, { useEffect, useState } from 'react'
import MainLayout from '../layout/MainLayout'
import BannerStore from '../section/StoreDetail/BannerStore'
import FoodStore from '../section/StoreDetail/FoodStore'
import { useAppDispatch, useAppSelector } from '../app/hooks'
import { useParams } from 'react-router-dom'
import { getDetailStore, selectDetailStore } from '../features/store/storeSlice'
import ProductAPI from '../api/product/api'
import NearBy from '../section/StoreDetail/NearBy'

const Shop = () => {
  const dispatch = useAppDispatch()
  const { storeId } = useParams()
  const { shopProfile } = useAppSelector(selectDetailStore)
  const [product, setProduct] = useState()

  useEffect(() => {
    if (storeId) {
      fetchDataList()
      fetchListProduct()
    }
  }, [storeId, dispatch])

  const fetchDataList = () => {
    dispatch(getDetailStore({ id: storeId }))
  }

  const fetchListProduct = async () => {
    const data = await ProductAPI.getListProductShop(storeId)
    setProduct(data.data.content)
  }

  return (
    <MainLayout>
      <BannerStore shopProfile={shopProfile} />
      <FoodStore listProduct={product} storeId={storeId} />
      <NearBy storeId={storeId} />
    </MainLayout>
  )
}

export default Shop
