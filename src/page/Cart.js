import React from 'react'

import { Box, Grid } from '@mui/material'
import { useLocation } from 'react-router-dom'
import MainLayout from '../layout/MainLayout'
import DetailCart from '../section/DetailCart'
import PaymentType from '../section/PaymentType'
import ShopPlace from '../section/ShopPlace'
import MainCard from '../ui-component/MainCard'

const Cart = () => {
  const location = useLocation()

  const data = location?.state?.data

  if (!data) {
    window.location.replace('/')
  } else {
    return (
      <MainLayout>
        <Box sx={{ padding: { xs: 2, sm: 4 } }}>
          <Grid container spacing={2}>
            <Grid item md={5} sx={12}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <MainCard>
                    <ShopPlace shopId={data?.shopId} />
                  </MainCard>
                </Grid>
                <Grid item xs={12}>
                  <MainCard>
                    <PaymentType />
                  </MainCard>
                </Grid>
              </Grid>
            </Grid>

            <Grid item md={7} sx={12} flex={1}>
              <MainCard>
                <DetailCart data={data} />
              </MainCard>
            </Grid>
          </Grid>
        </Box>
      </MainLayout>
    )
  }
}

export default Cart
