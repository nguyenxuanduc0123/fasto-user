import React from 'react'
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline'
import { Box, Stack, Typography } from '@mui/material'
import { colors } from '../app/constant'
import MainButton from '../ui-component/Button/MainButton'
import useCountdown from '../hooks/useCountdown'
import { useNavigate } from 'react-router-dom'

const Succeed = () => {
  const navigate = useNavigate()

  const redirectPage = () => {
    navigate('/')
  }

  const countdown = useCountdown(5, redirectPage)

  return (
    <Box sx={{ height: '100vh' }}>
      <Stack
        alignItems="center"
        sx={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
        }}
      >
        <CheckCircleOutlineIcon sx={{ color: 'green', fontSize: '100px' }} />
        <Typography sx={{ color: colors.mainColor }} variant="h4">
          Thanh toán thành công!
        </Typography>
        <Typography variant="h6" mb={1}>
          Bạn sẽ dược chuyển đến trang chủ sau {countdown} giây
        </Typography>
        <MainButton onClick={redirectPage}>Về trang chủ</MainButton>
      </Stack>
    </Box>
  )
}

export default Succeed
