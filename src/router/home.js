import Home from '../page/Home'

const home = {
  path: '/',
  element: <Home />,
}

export default home
