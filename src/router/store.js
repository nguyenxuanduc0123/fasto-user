import Store from '../page/Store'

const store = {
  path: '/store/:storeId',
  element: <Store />,
}

export default store
