import Cart from '../page/Cart'

const cart = {
  path: '/cart',
  element: <Cart />,
}

export default cart
