import BillDetail from '../page/BillDetail'

const billDetail = {
  path: '/bill/:billId',
  element: <BillDetail />,
}

export default billDetail
