import ForgotPass from '../page/ForgotPass'

const forgotPassword = {
  path: '/forgotPassword',
  element: <ForgotPass />,
}

export default forgotPassword
