import Voucher from '../page/Voucher'

const voucher = {
  path: '/voucher/:voucherId',
  element: <Voucher />,
}

export default voucher
