import account from './account'
import billDetail from './bill'
import cart from './cart'
import forgotPassword from './forgotPassword'
import home from './home'
import store from './store'
import succeed from './succeed'
import voucher from './voucher'

const appRouter = [
  home,
  store,
  account,
  voucher,
  cart,
  billDetail,
  succeed,
  forgotPassword,
]

export default appRouter
