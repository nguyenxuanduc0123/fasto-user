import Account from '../page/Account'

const account = {
  path: '/account',
  element: <Account />,
}

export default account
