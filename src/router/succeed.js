import Succeed from '../page/Succeed'

const succeed = {
  path: '/succeed',
  element: <Succeed />,
}

export default succeed
