import { Grid, IconButton, Stack } from '@mui/material'
import React from 'react'
import FacebookIcon from '@mui/icons-material/Facebook'
import InstagramIcon from '@mui/icons-material/Instagram'

const Footer = () => {
  return (
    <div className="w-full bg-gradient-to-r from-orange-600 to-yellow-600 px-12 pt-12 pb-6 text-white">
      <div className="w-28 mb-2">
        <img src="/images/logo.png" alt="" />
      </div>

      <div className="content py-6 border-y">
        <Grid container>
          <Grid item md={3}>
            <span>Về FastO</span>
          </Grid>
          <Grid item md={3}>
            <span>Mở quán trên FastO</span>
          </Grid>
          <Grid item md={3}>
            <Stack rowGap={1}>
              <span>Trung tâm hỗ trợ</span>
              <span>Câu hỏi thường gặp</span>
            </Stack>
          </Grid>
          <Grid item md={3}>
            <Stack direction="row" rowGap={2}>
              <IconButton size="large" sx={{ padding: 0, margin: 0 }}>
                <FacebookIcon />
              </IconButton>

              <IconButton className="" sx={{ padding: 0, margin: 0 }}>
                <InstagramIcon />
              </IconButton>
            </Stack>
          </Grid>
        </Grid>
      </div>

      <div className="copyright flex justify-between items-center pt-6">
        <span className="w-full text-center">
          Ⓒ 2023 FASTO - A New Way To Order Food. All rights reserved.
        </span>
      </div>
    </div>
  )
}

export default Footer
