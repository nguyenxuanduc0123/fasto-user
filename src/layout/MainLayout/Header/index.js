import { Button } from '@mui/material'
import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { colors } from '../../../app/constant'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import {
  getAccount,
  selectAccount,
} from '../../../features/account/accountSlice'
import { selectAuthResponse } from '../../../features/auth/authSlice'
import { getListCart, selectListCart } from '../../../features/cart/cartSlice'
import {
  openLoginModal,
  openRegisterModal,
} from '../../../features/modalLogin/modalLoginSlice'
import { getListStore } from '../../../features/store/storeSlice'
import CartHeader from '../../../section/Cart/CartHeader'
import MainButton from '../../../ui-component/Button/MainButton'
import StorageUtil, { STORAGE_KEY } from '../../../util/storage'
import ProfileSection from './ProfileSection'

const Header = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()

  const response = useAppSelector(selectAuthResponse)
  const cartInfo = useAppSelector(selectListCart)
  const accountInfo = useAppSelector(selectAccount)

  useEffect(() => {
    fetchDataList({})
  }, [dispatch])

  const fetchDataList = () => {
    dispatch(getListStore())
  }

  const jwt = StorageUtil.get(STORAGE_KEY.JWT)

  const getCart = () => {
    dispatch(getListCart())
  }

  const getAccountInfo = () => {
    dispatch(getAccount())
  }

  useEffect(() => {
    if (response.status === 'successed' || jwt) {
      getCart()
      getAccountInfo()
    }
  }, [response.status, jwt])

  return (
    <div className="w-full z-50 flex justify-between items-center px-12 py-2 border-b-2 fixed bg-white top-0">
      <div className="logo w-16 cursor-pointer" onClick={() => navigate('/')}>
        <img src="/images/logo2.png" alt="" />
      </div>

      <div className="header-info">
        <CartHeader cartInfo={cartInfo} />

        {accountInfo || jwt ? (
          <ProfileSection accountInfo={accountInfo} />
        ) : (
          <>
            <MainButton
              onClick={() => {
                dispatch(openLoginModal())
              }}
            >
              Đăng nhập
            </MainButton>
            <Button
              onClick={() => {
                dispatch(openRegisterModal())
              }}
              sx={{
                color: colors.mainColor,
                paddingY: 1,
                ml: 2,
              }}
            >
              Đăng ký
            </Button>
          </>
        )}
      </div>
    </div>
  )
}

export default Header
