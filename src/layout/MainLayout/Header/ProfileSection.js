import { useEffect, useRef, useState } from 'react'

// material-ui
import {
  Avatar,
  Box,
  Chip,
  ClickAwayListener,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Paper,
  Popper,
  Typography,
} from '@mui/material'

// project imports

// assets
import { IconLogout, IconSettings, IconUser } from '@tabler/icons'
import { colors } from '../../../app/constant'
import { useNavigate } from 'react-router-dom'

// ==============================|| PROFILE MENU ||============================== //

const ProfileSection = ({ accountInfo }) => {
  const navigate = useNavigate()
  const [open, setOpen] = useState(false)
  /**
   * anchorRef is used on different componets and specifying one type leads to other components throwing an error
   * */
  const anchorRef = useRef(null)
  const handleLogout = async () => {
    localStorage.clear()
    window.location.reload()
  }

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }
    setOpen(false)
  }

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen)
  }

  const prevOpen = useRef(open)
  useEffect(() => {
    if (anchorRef.current && prevOpen.current === true && open === false) {
      anchorRef.current.focus()
    }

    prevOpen.current = open
  }, [open])

  return (
    <>
      <Chip
        sx={{
          height: '48px',
          alignItems: 'center',
          borderRadius: '27px',
          transition: 'all .2s ease-in-out',
          '& .MuiChip-label': {
            lineHeight: 0,
          },
        }}
        icon={
          <Avatar
            src={accountInfo?.userImage}
            sx={{
              margin: '8px 0 8px 8px !important',
              cursor: 'pointer',
            }}
            ref={anchorRef}
            aria-controls={open ? 'menu-list-grow' : undefined}
            aria-haspopup="true"
            color="inherit"
          />
        }
        label={
          <IconSettings
            stroke={1.5}
            size="1.5rem"
            color={colors.primaryColor}
          />
        }
        variant="outlined"
        ref={anchorRef}
        aria-controls={open ? 'menu-list-grow' : undefined}
        aria-haspopup="true"
        onClick={handleToggle}
        color="primary"
      />
      <Popper
        placement="bottom-end"
        open={open}
        anchorEl={anchorRef.current}
        popperOptions={{
          modifiers: [
            {
              name: 'offset',
              options: {
                offset: [0, 14],
              },
            },
          ],
        }}
        sx={{
          border: '1px solid #333',
          borderRadius: '16px',
          overflow: 'hidden',
          boxShadow: '2px 5px 15px rgba(0,0,0,0.7)',
        }}
      >
        {() => (
          <Paper>
            <ClickAwayListener onClickAway={handleClose}>
              <Box>
                <Box sx={{ p: 2 }}>
                  <List
                    component="nav"
                    sx={{
                      width: '100%',
                      maxWidth: 350,
                      minWidth: '240px',
                      borderRadius: '12px',
                      '.MuiButtonBase-root': {
                        borderLeft: `6px solid transparent`,
                      },
                      '.MuiListItemIcon-root': {
                        minWidth: '35px',
                      },
                    }}
                  >
                    <ListItemButton
                      onClick={() => navigate('/account')}
                      sx={{
                        '&:hover ': {
                          borderLeft: `6px solid ${colors.mainColor} !important`,
                        },
                      }}
                    >
                      <ListItemIcon>
                        <IconUser stroke={1.5} size="1.3rem" />
                      </ListItemIcon>
                      <ListItemText
                        primary={
                          <Typography variant="body2">
                            Thông tin cá nhân
                          </Typography>
                        }
                      />
                    </ListItemButton>

                    <ListItemButton
                      onClick={handleLogout}
                      sx={{
                        '&:hover ': {
                          backgroundColor: ``,
                          borderLeft: `6px solid ${colors.mainColor} !important`,
                        },
                      }}
                    >
                      <ListItemIcon>
                        <IconLogout stroke={1.5} size="1.3rem" />
                      </ListItemIcon>
                      <ListItemText
                        primary={
                          <Typography variant="body2">Đăng xuất</Typography>
                        }
                      />
                    </ListItemButton>
                  </List>
                </Box>
              </Box>
            </ClickAwayListener>
          </Paper>
        )}
      </Popper>
    </>
  )
}

export default ProfileSection
