import React from 'react'
import ModalLogin from '../../section/ModalLogin'
import ModalRegister from '../../section/ModalRegister'
import Footer from './Footer'
import Header from './Header'

const MainLayout = ({ children }) => {
  return (
    <main className="mt-[80px]">
      <Header />

      <div>{children}</div>

      <Footer />
      <ModalLogin />
      <ModalRegister />
    </main>
  )
}

export default MainLayout
