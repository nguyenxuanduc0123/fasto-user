import React from 'react'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import appRouter from './router'

const App = () => {
  const routerDOM = createBrowserRouter(appRouter)

  return <RouterProvider router={routerDOM} />
}

export default App
