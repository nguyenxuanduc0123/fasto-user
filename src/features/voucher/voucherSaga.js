import { all, call, put, takeLatest } from 'redux-saga/effects'
import VoucherAPI from '../../api/voucher/api'
import {
  failGetDetailVoucher,
  failGetListVoucher,
  getDetailVoucher,
  getListVoucher,
  successGetDetailVoucher,
  successGetListVoucher,
} from './voucherSlice'

function* _getListVoucher({ payload }) {
  try {
    const response = yield call(VoucherAPI.getListVoucher, payload)
    const result = response.data
    yield put(successGetListVoucher(result))
  } catch (error) {
    yield put(failGetListVoucher())
  }
}

function* _getDetailVoucher({ payload }) {
  try {
    const response = yield call(VoucherAPI.getDetailVoucher, payload)
    const result = response.data

    yield put(successGetDetailVoucher(result))
  } catch (error) {
    yield put(failGetDetailVoucher())
  }
}

export function* voucherSaga() {
  yield all([takeLatest(getListVoucher, _getListVoucher)])
  yield all([takeLatest(getDetailVoucher, _getDetailVoucher)])
}
