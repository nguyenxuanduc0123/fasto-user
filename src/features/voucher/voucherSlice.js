import { createSlice } from '@reduxjs/toolkit'
import { INIT_LIST, INIT_QUERY, INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getListVoucher: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
  getDetailVoucher: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
}

export const voucherSlice = createSlice({
  name: '@voucher',
  initialState,
  reducers: {
    // GET LIST VOUCHER
    getListVoucher: (state, action) => {
      state.getListVoucher.status.processing = !!action.payload
      state.getListVoucher.status.success = false
      state.getListVoucher.status.fail = false
      state.getListVoucher.query = action.payload
    },
    successGetListVoucher: (state, action) => {
      state.getListVoucher.list = action.payload
      state.getListVoucher.status.success = true
      state.getListVoucher.status.processing = false
    },
    failGetListVoucher: (state) => {
      state.getListVoucher.status.fail = true
      state.getListVoucher.status.processing = false
    },

    getDetailVoucher: (state, action) => {
      state.getDetailVoucher.status.processing = !!action.payload
      state.getDetailVoucher.status.fail = false
      state.getDetailVoucher.status.success = false
    },
    stopGetDetailVoucher: (state) => {
      state.getDetailVoucher.status.processing = false
    },
    failGetDetailVoucher: (state) => {
      state.getDetailVoucher.state.fail = true
    },
    successGetDetailVoucher: (state, action) => {
      state.getDetailVoucher.detail = action.payload
      state.getDetailVoucher.status.success = true
    },
  },
})

export const {
  getListVoucher,
  successGetListVoucher,
  failGetListVoucher,
  getDetailVoucher,
  stopGetDetailVoucher,
  failGetDetailVoucher,
  successGetDetailVoucher,
} = voucherSlice.actions

export const selectListVoucher = (state) => state.voucher.getListVoucher.list
export const selectListVoucherQuery = (state) =>
  state.voucher.getListVoucher.query
export const selectDetailVoucher = (state) =>
  state.voucher.getDetailVoucher.detail

export default voucherSlice.reducer
