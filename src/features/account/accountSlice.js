import { createSlice } from '@reduxjs/toolkit'
import { INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getAccount: {
    status: { ...INIT_REQUEST_STATUS },
    detail: undefined,
  },
  editAccount: {
    status: { ...INIT_REQUEST_STATUS },
  },
  changeAvatar: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const accountSlice = createSlice({
  name: '@account',
  initialState,
  reducers: {
    getAccount: (state, action) => {
      state.getAccount.status.processing = !!action.payload
      state.getAccount.status.fail = false
      state.getAccount.status.success = false
    },
    stopGetAccount: (state) => {
      state.getAccount.status.processing = false
    },
    failGetAccount: (state) => {
      state.getAccount.state.fail = true
    },
    successGetAccount: (state, action) => {
      state.getAccount.detail = action.payload
      state.getAccount.status.success = true
    },

    editAccount: (state, action) => {
      state.editAccount.status.processing = !!action.payload
      state.editAccount.status.fail = false
      state.editAccount.status.success = false
    },
    stopEditAccount: (state) => {
      state.editAccount.status.processing = false
    },
    failEditAccount: (state) => {
      state.editAccount.status.fail = true
      state.editAccount.status.processing = false
    },
    successEditAccount: (state) => {
      state.editAccount.status.success = true
      state.editAccount.status.processing = false
    },

    changeAvatar: (state, action) => {
      state.changeAvatar.status.processing = !!action.payload
      state.changeAvatar.status.fail = false
      state.changeAvatar.status.success = false
    },
    stopChangeAvatar: (state) => {
      state.changeAvatar.status.processing = false
    },
    failChangeAvatar: (state) => {
      state.changeAvatar.status.fail = true
      state.changeAvatar.status.processing = false
    },
    successChangeAvatar: (state) => {
      state.changeAvatar.status.success = true
      state.changeAvatar.status.processing = false
    },
  },
})

export const {
  getAccount,
  stopGetAccount,
  failGetAccount,
  successGetAccount,
  editAccount,
  stopEditAccount,
  failEditAccount,
  successEditAccount,
  changeAvatar,
  stopChangeAvatar,
  failChangeAvatar,
  successChangeAvatar,
} = accountSlice.actions

export const selectAccount = (state) => state.account.getAccount.detail

export default accountSlice.reducer
