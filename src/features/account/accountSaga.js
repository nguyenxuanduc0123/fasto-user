import { all, call, put, takeLatest } from 'redux-saga/effects'
import AccountAPI from '../../api/account/api'
import {
  changeAvatar,
  editAccount,
  failChangeAvatar,
  failEditAccount,
  failGetAccount,
  getAccount,
  successChangeAvatar,
  successEditAccount,
  successGetAccount,
} from './accountSlice'

function* _getAccount({ payload }) {
  try {
    const response = yield call(AccountAPI.getAccount, payload)
    const result = response.data

    yield put(successGetAccount(result))
  } catch (error) {
    yield put(failGetAccount())
  }
}

function* _editAccount({ payload }) {
  try {
    yield call(AccountAPI.editAccount, payload)
    yield put(successEditAccount())
    yield put(getAccount())
  } catch (error) {
    yield put(failEditAccount())
  }
}

function* _changeAvatar({ payload }) {
  try {
    yield call(AccountAPI.changeAvatar, payload)
    yield put(successChangeAvatar())
    yield put(getAccount())
  } catch (error) {
    yield put(failChangeAvatar())
  }
}

export function* accountSaga() {
  yield all([
    takeLatest(getAccount, _getAccount),
    takeLatest(editAccount, _editAccount),
    takeLatest(changeAvatar, _changeAvatar),
  ])
}
