import { all, call, put, takeLatest } from 'redux-saga/effects'
import ProductAPI from '../../api/product/api'
import {
  failGetListProduct,
  getListProduct,
  successGetListProduct,
} from './productSlice'

function* _getListProduct({ payload }) {
  try {
    const response = yield call(ProductAPI.getListProduct, payload)
    const result = response.data
    yield put(successGetListProduct(result))
  } catch (error) {
    yield put(failGetListProduct())
  }
}

export function* productSaga() {
  yield all([takeLatest(getListProduct, _getListProduct)])
}
