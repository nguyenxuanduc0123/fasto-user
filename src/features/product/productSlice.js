import { createSlice } from '@reduxjs/toolkit'
import { INIT_LIST, INIT_QUERY, INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getListProduct: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
}

export const productSlice = createSlice({
  name: '@product',
  initialState,
  reducers: {
    // GET LIST PRODUCT
    getListProduct: (state, action) => {
      state.getListProduct.status.processing = !!action.payload
      state.getListProduct.status.success = false
      state.getListProduct.status.fail = false
      state.getListProduct.query = action.payload
    },
    successGetListProduct: (state, action) => {
      state.getListProduct.list = action.payload
      state.getListProduct.status.success = true
      state.getListProduct.status.processing = false
    },
    failGetListProduct: (state) => {
      state.getListProduct.status.fail = true
      state.getListProduct.status.processing = false
    },
  },
})

export const { getListProduct, successGetListProduct, failGetListProduct } =
  productSlice.actions

export const selectListProduct = (state) => state.product.getListProduct.list
export const selectListProductQuery = (state) =>
  state.product.getListProduct.query

export default productSlice.reducer
