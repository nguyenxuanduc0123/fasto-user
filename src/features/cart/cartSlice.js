import { createSlice } from '@reduxjs/toolkit'
import { INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  addToCart: {
    status: { ...INIT_REQUEST_STATUS },
  },
  getListCart: {
    list: {},
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const cartSlice = createSlice({
  name: '@cart',
  initialState,
  reducers: {
    // ADD TO CART
    addToCart: (state, action) => {
      state.addToCart.status.processing = !!action.payload
      state.addToCart.status.fail = false
      state.addToCart.status.success = false
    },
    stopAddToCart: (state) => {
      state.addToCart.status.processing = false
    },
    failAddToCart: (state) => {
      state.addToCart.status.fail = true
      state.addToCart.status.processing = false
    },
    successAddToCart: (state) => {
      state.addToCart.status.success = true
      state.addToCart.status.processing = false
    },

    // GET LIST CART
    getListCart: (state, action) => {
      state.getListCart.status.processing = !!action.payload
      state.getListCart.status.success = false
      state.getListCart.status.fail = false
      state.getListCart.query = action.payload
    },
    successGetListCart: (state, action) => {
      state.getListCart.list = action.payload
      state.getListCart.status.success = true
      state.getListCart.status.processing = false
    },
    failGetListCart: (state) => {
      state.getListCart.status.fail = true
      state.getListCart.status.processing = false
    },
  },
})

export const {
  addToCart,
  stopAddToCart,
  failAddToCart,
  successAddToCart,
  getListCart,
  successGetListCart,
  failGetListCart,
} = cartSlice.actions

export const selectListCart = (state) => state.cart.getListCart.list
export const selectStatusListCart = (state) => state.cart.getListCart.status

export default cartSlice.reducer
