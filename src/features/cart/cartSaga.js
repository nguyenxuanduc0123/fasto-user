import { all, call, put, takeLatest } from 'redux-saga/effects'
import CartAPI from '../../api/cart/api'
import { closeAllModal } from '../modalLogin/modalLoginSlice'
import {
  addToCart,
  failAddToCart,
  failGetListCart,
  getListCart,
  successAddToCart,
  successGetListCart,
} from './cartSlice'

function* _getListCart({ payload }) {
  try {
    const response = yield call(CartAPI.getListCart, payload)
    const result = response.data
    yield put(successGetListCart(result))
  } catch (error) {
    yield put(failGetListCart())
  }
}

function* _addToCart({ payload }) {
  try {
    const response = yield call(CartAPI.addToCart, payload)
    yield put(successAddToCart())
    yield put(successGetListCart(response.data))
    yield put(closeAllModal())
  } catch (error) {
    yield put(failAddToCart())
  }
}

export function* cartSaga() {
  yield all([
    takeLatest(addToCart, _addToCart),
    takeLatest(getListCart, _getListCart),
  ])
}
