import { createSlice } from '@reduxjs/toolkit'

export const initialState = {
  loginModal: false,
  registerModal: false,
  verifyModal: false,
  foodModal: false,
  qrModal: false,
  ratingModal: false,
  feedbackModal: false,
}

export const loginModalSlice = createSlice({
  name: '@modalLogin',
  initialState,
  reducers: {
    openLoginModal: (state) => {
      state.loginModal = true
    },
    openRegisterModal: (state) => {
      state.registerModal = true
    },
    openVerifyModal: (state) => {
      state.verifyModal = true
    },
    openFoodModal: (state) => {
      state.foodModal = true
    },
    openQRModal: (state) => {
      state.qrModal = true
    },
    openRatingModal: (state) => {
      state.ratingModal = true
    },
    openFeedbackModal: (state) => {
      state.feedbackModal = true
    },
    closeAllModal: (state) => {
      state.registerModal = false
      state.loginModal = false
      state.verifyModal = false
      state.foodModal = false
      state.qrModal = false
      state.ratingModal = false
      state.feedbackModal = false
    },
  },
})

export const {
  openLoginModal,
  openRegisterModal,
  openVerifyModal,
  openFoodModal,
  openQRModal,
  openRatingModal,
  openFeedbackModal,
  closeAllModal,
} = loginModalSlice.actions

export const selectLoginModal = (state) => state.modalLogin.loginModal
export const selectRegisterModal = (state) => state.modalLogin.registerModal
export const selectVerifyModal = (state) => state.modalLogin.verifyModal
export const selectFoodModal = (state) => state.modalLogin.foodModal
export const selectQRModal = (state) => state.modalLogin.qrModal
export const selectRatingModal = (state) => state.modalLogin.ratingModal
export const selectFeedbackModal = (state) => state.modalLogin.feedbackModal

export default loginModalSlice.reducer
