import { createSlice } from '@reduxjs/toolkit'
import { INIT_LIST, INIT_QUERY, INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getListStore: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },

  getDetailStore: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  getListFavoriteStore: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
  addFavoriteStore: {
    status: { ...INIT_REQUEST_STATUS },
  },
  removeFavoriteStore: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const storeSlice = createSlice({
  name: '@store',
  initialState,
  reducers: {
    // GET LIST STORE
    getListStore: (state, action) => {
      state.getListStore.status.processing = !!action.payload
      state.getListStore.status.success = false
      state.getListStore.status.fail = false
      state.getListStore.query = action.payload
    },
    successGetListStore: (state, action) => {
      state.getListStore.list = action.payload
      state.getListStore.status.success = true
      state.getListStore.status.processing = false
    },
    failGetListStore: (state) => {
      state.getListStore.status.fail = true
      state.getListStore.status.processing = false
    },

    getDetailStore: (state, action) => {
      state.getDetailStore.status.processing = !!action.payload
      state.getDetailStore.status.fail = false
      state.getDetailStore.status.success = false
    },
    stopGetDetailStore: (state) => {
      state.getDetailStore.status.processing = false
    },
    failGetDetailStore: (state) => {
      state.getDetailStore.state.fail = true
    },
    successGetDetailStore: (state, action) => {
      state.getDetailStore.detail = action.payload
      state.getDetailStore.status.success = true
    },

    getListFavoriteStore: (state, action) => {
      state.getListFavoriteStore.status.processing = !!action.payload
      state.getListFavoriteStore.status.success = false
      state.getListFavoriteStore.status.fail = false
      state.getListFavoriteStore.query = action.payload
    },
    successGetListFavoriteStore: (state, action) => {
      state.getListFavoriteStore.list = action.payload
      state.getListFavoriteStore.status.success = true
      state.getListFavoriteStore.status.processing = false
    },
    failGetListFavoriteStore: (state) => {
      state.getListFavoriteStore.status.fail = true
      state.getListFavoriteStore.status.processing = false
    },

    addFavoriteStore: (state, action) => {
      state.addFavoriteStore.status.processing = !!action.payload
      state.addFavoriteStore.status.success = false
      state.addFavoriteStore.status.fail = false
      state.addFavoriteStore.query = action.payload
    },
    successAddFavoriteStore: (state, action) => {
      state.addFavoriteStore.list = action.payload
      state.addFavoriteStore.status.success = true
      state.addFavoriteStore.status.processing = false
    },
    failAddFavoriteStore: (state) => {
      state.addFavoriteStore.status.fail = true
      state.addFavoriteStore.status.processing = false
    },

    removeFavoriteStore: (state, action) => {
      state.removeFavoriteStore.status.processing = !!action.payload
      state.removeFavoriteStore.status.success = false
      state.removeFavoriteStore.status.fail = false
      state.removeFavoriteStore.query = action.payload
    },
    successRemoveFavoriteStore: (state, action) => {
      state.removeFavoriteStore.list = action.payload
      state.removeFavoriteStore.status.success = true
      state.removeFavoriteStore.status.processing = false
    },
    failRemoveFavoriteStore: (state) => {
      state.removeFavoriteStore.status.fail = true
      state.removeFavoriteStore.status.processing = false
    },
  },
})

export const {
  getListStore,
  successGetListStore,
  failGetListStore,
  getDetailStore,
  stopGetDetailStore,
  failGetDetailStore,
  successGetDetailStore,
  getListFavoriteStore,
  successGetListFavoriteStore,
  failGetListFavoriteStore,
  addFavoriteStore,
  successAddFavoriteStore,
  failAddFavoriteStore,
  removeFavoriteStore,
  successRemoveFavoriteStore,
  failRemoveFavoriteStore,
} = storeSlice.actions

export const selectListStore = (state) => state.store.getListStore.list
export const selectListStoreQuery = (state) => state.store.getListStore.query
export const selectDetailStore = (state) => state.store.getDetailStore.detail
export const selectFavoriteStore = (state) =>
  state.store.getListFavoriteStore.list

export default storeSlice.reducer
