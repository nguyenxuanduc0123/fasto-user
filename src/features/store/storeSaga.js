import { all, call, put, takeLatest } from 'redux-saga/effects'
import StoreAPI from '../../api/store/api'
import {
  addFavoriteStore,
  failAddFavoriteStore,
  failGetDetailStore,
  failGetListFavoriteStore,
  failGetListStore,
  failRemoveFavoriteStore,
  getDetailStore,
  getListFavoriteStore,
  getListStore,
  removeFavoriteStore,
  successAddFavoriteStore,
  successGetDetailStore,
  successGetListFavoriteStore,
  successGetListStore,
  successRemoveFavoriteStore,
} from './storeSlice'

function* _getListStore({ payload }) {
  try {
    const response = yield call(StoreAPI.getListStore, payload)
    const result = response.data
    yield put(successGetListStore(result))
  } catch (error) {
    yield put(failGetListStore())
  }
}

function* _getDetailStore({ payload }) {
  try {
    const response = yield call(StoreAPI.getDetailStore, payload)
    const result = response.data

    yield put(successGetDetailStore(result))
  } catch (error) {
    yield put(failGetDetailStore())
  }
}

function* _getListFavoriteStore({ payload }) {
  try {
    const response = yield call(StoreAPI.getListFavoriteStore, payload)
    const result = response.data
    yield put(successGetListFavoriteStore(result))
  } catch (error) {
    yield put(failGetListFavoriteStore())
  }
}

function* _addFavoriteStore({ payload }) {
  try {
    const response = yield call(StoreAPI.addFavoriteStore, payload)
    const result = response.data
    yield put(successAddFavoriteStore(result))
    yield put(getListStore())
  } catch (error) {
    yield put(failAddFavoriteStore())
  }
}

function* _removeFavoriteStore({ payload }) {
  try {
    const response = yield call(StoreAPI.removeFavoriteStore, payload)
    const result = response.data
    yield put(successRemoveFavoriteStore(result))
    yield put(getListStore())
  } catch (error) {
    yield put(failRemoveFavoriteStore())
  }
}

export function* storeSaga() {
  yield all([takeLatest(getListStore, _getListStore)])
  yield all([takeLatest(getDetailStore, _getDetailStore)])
  yield all([takeLatest(getListFavoriteStore, _getListFavoriteStore)])
  yield all([takeLatest(addFavoriteStore, _addFavoriteStore)])
  yield all([takeLatest(removeFavoriteStore, _removeFavoriteStore)])
}
