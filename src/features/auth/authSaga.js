import { all, call, put, takeLatest } from 'redux-saga/effects'
import UserAPI from '../../api/authentication/api'
import StorageUtil, { STORAGE_KEY } from '../../util/storage'
import { activeAccount, signIn, signInGoogle } from './authAction'
import {
  changePassword,
  changeStatus,
  changeStatusActive,
  failChangePassword,
  register,
  successChangePassword,
  successRegister,
} from './authSlice'

import { closeAllModal } from '../modalLogin/modalLoginSlice'
import { getAccount } from '../account/accountSlice'

function* _signIn({ payload }) {
  try {
    yield put(changeStatus({ status: 'pending' }))
    const response = yield call(UserAPI.signIn, payload)
    yield put(changeStatus({ status: 'fullfiledd' }))

    StorageUtil.set(STORAGE_KEY.JWT, response.data.id_token)

    if (response.data.id_token) {
      yield put(changeStatus({ status: 'successed' }))

      yield put(getAccount())

      yield put(closeAllModal())
    }
  } catch (e) {
    yield put(
      changeStatus({
        status: 'rejected',
        error: e.response?.data?.message,
      })
    )
  }
}

function* _signInGoogle({ payload }) {
  try {
    yield put(changeStatus({ status: 'pending' }))
    const response = yield call(UserAPI.signInGoogle, payload)
    yield put(changeStatus({ status: 'fullfiledd' }))

    StorageUtil.set(STORAGE_KEY.JWT, response.data.id_token)

    if (response.data.id_token) {
      yield put(changeStatus({ status: 'successed' }))

      yield put(closeAllModal())
    }
  } catch (e) {
    yield put(
      changeStatus({
        status: 'rejected',
        error: e.response?.data?.message,
      })
    )
  }
}

function* _changePassword({ payload }) {
  try {
    yield call(UserAPI.changePassword, payload)

    yield put(successChangePassword())
  } catch (e) {
    yield put(failChangePassword())
  }
}

function* _register({ payload }) {
  try {
    yield call(UserAPI.register, payload)

    yield put(successRegister())
  } catch (e) {
    yield put(successRegister())
  }
}

function* _activeAccount({ payload }) {
  try {
    const response = yield call(UserAPI.activeAccount, payload)

    if (response.data.id_token) {
      yield put(changeStatusActive({ status: 'successed' }))
      yield put(closeAllModal())
    }
  } catch (e) {
    yield put(
      changeStatusActive({
        status: 'rejected',
        error: e.response?.data?.message,
      })
    )
  }
}

export default function* authSaga() {
  yield all([
    takeLatest(signIn, _signIn),
    takeLatest(signInGoogle, _signInGoogle),
    takeLatest(activeAccount, _activeAccount),
    takeLatest(changePassword, _changePassword),
    takeLatest(register, _register),
  ])
}
