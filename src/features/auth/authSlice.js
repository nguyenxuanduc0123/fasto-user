import { createSlice } from '@reduxjs/toolkit'
import { INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  user: {},
  response: {
    status: 'idle',
    error: '',
  },
  responseActive: {
    status: '',
    error: '',
  },
  changePassword: {
    status: { ...INIT_REQUEST_STATUS },
  },
  register: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const authSlice = createSlice({
  name: '@authentication',
  initialState,
  reducers: {
    changeStatus: (state, action) => {
      state.response.status = action.payload.status
      state.response.error = action.payload.error
    },
    changeStatusActive: (state, action) => {
      state.responseActive.status = action.payload.status
      state.responseActive.error = action.payload.error
    },
    changePassword: (state, action) => {
      state.changePassword.status.processing = !!action.payload
      state.changePassword.status.fail = false
      state.changePassword.status.success = false
    },
    stopChangePassword: (state) => {
      state.changePassword.status.processing = false
    },
    failChangePassword: (state) => {
      state.changePassword.status.fail = true
      state.changePassword.status.processing = false
    },
    successChangePassword: (state) => {
      state.changePassword.status.success = true
      state.changePassword.status.processing = false
    },

    register: (state, action) => {
      state.register.status.processing = !!action.payload
      state.register.status.fail = false
      state.register.status.success = false
    },
    stopRegister: (state) => {
      state.register.status.processing = false
    },
    failRegister: (state) => {
      state.register.status.fail = true
      state.register.status.processing = false
    },
    successRegister: (state) => {
      state.register.status.success = true
      state.register.status.processing = false
    },
  },
})

export const {
  changeStatus,
  changeStatusActive,
  changePassword,
  stopChangePassword,
  failChangePassword,
  successChangePassword,

  register,
  stopRegister,
  failRegister,
  successRegister,
} = authSlice.actions

export const selectAuthResponse = (state) => state.auth.response
export const selectAuthResponseActive = (state) => state.auth.responseActive
export const selectChangePasswordStatus = (state) =>
  state.auth.changePassword.status
export const selectRegisterStatus = (state) => state.auth.register.status
export default authSlice.reducer
