import { createAction } from '@reduxjs/toolkit'

const signIn = createAction('auth/signIn')
const signInGoogle = createAction('auth/signInGoogle')
const activeAccount = createAction('auth/activeAccount')

export { signIn, signInGoogle, activeAccount }
