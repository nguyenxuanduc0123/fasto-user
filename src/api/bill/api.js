import appAxios from '../constant/axios-config'
import { userVersion } from '../constant/version'

export const API_BILL = {
  CREATE_BILL: `${userVersion}/management/bill/create`,
  GET_LIST_BILL: `${userVersion}/management/bill`,
  GET_DETAIL_BILL: (id) => `${userVersion}/management/bill/${id}`,
}

export default class BillAPI {
  static createBill = (params = {}) =>
    appAxios.post(API_BILL.CREATE_BILL, params)

  static getListBill = (params = {}) =>
    appAxios.get(API_BILL.GET_LIST_BILL, params)

  static getDetailBill = (params = {}) =>
    appAxios.get(API_BILL.GET_DETAIL_BILL(params.id))
}
