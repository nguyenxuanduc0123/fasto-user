import appAxios from '../constant/axios-config'
import { userVersion } from '../constant/version'

export const API_PRODUCT = {
  GET_LIST_PRODUCT: `${userVersion}/management/product`,
  GET_TOP_PRODUCT: `${userVersion}/management/product/top-products`,
  GET_LIST_PRODUCT_SHOP: (id) => `${userVersion}/management/product/${id}`,
}

export default class ProductAPI {
  static getListProduct = (params = {}) =>
    appAxios.get(API_PRODUCT.GET_LIST_PRODUCT, params)

  static getTopProduct = (params = {}) =>
    appAxios.get(API_PRODUCT.GET_TOP_PRODUCT, params)

  static getListProductShop = (id) =>
    appAxios.get(API_PRODUCT.GET_LIST_PRODUCT_SHOP(id))
}
