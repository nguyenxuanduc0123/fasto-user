import appAxios from '../constant/axios-config'
import { userVersion } from '../constant/version'

export const API_ACCOUNT = {
  GET_ACCOUNT: `${userVersion}/management/information`,
  EDIT_ACCOUNT: `${userVersion}/management/information`,
  CHANGE_AVATAR: `${userVersion}/management/update-avatar`,
}

export default class AccountAPI {
  static getAccount = (params = {}) =>
    appAxios.get(API_ACCOUNT.GET_ACCOUNT, params)

  static editAccount = (params = {}) =>
    appAxios.post(API_ACCOUNT.EDIT_ACCOUNT, params)

  static changeAvatar = (img) => appAxios.patch(API_ACCOUNT.CHANGE_AVATAR, img)
}
