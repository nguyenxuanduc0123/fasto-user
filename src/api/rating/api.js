import appAxios from '../constant/axios-config'
import { userVersion } from '../constant/version'

export const API_RATING = {
  CREATE_RATING: `${userVersion}/management/rating`,
  GET_RATING: (id) => `${userVersion}/management/rating/${id}`,
}

export default class RatingAPI {
  static createRating = (params = {}) =>
    appAxios.post(API_RATING.CREATE_RATING, params)

  static getRating = (params = {}) =>
    appAxios.get(API_RATING.GET_RATING(params.id), params)
}
