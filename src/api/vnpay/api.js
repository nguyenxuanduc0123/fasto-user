import appAxios from '../constant/axios-config'
import { userVersion } from '../constant/version'

export const API_VNPAY = {
  PAYMENT: `${userVersion}/management/vn-pay/vnpay/payment-url`,
}

export default class VnpayAPI {
  static payment = (params = {}) => appAxios.post(API_VNPAY.PAYMENT, params)
}
