import appAxios from '../constant/axios-config'
import { userVersion } from '../constant/version'

export const API_USER = {
  SIGN_IN: `${userVersion}/authenticate`,
  CHANGE_PASSWORD: `${userVersion}/authentication/change-password`,
  REGISTER: `${userVersion}/authenticate/register`,
  ACTIVE_ACCOUNT: `${userVersion}/authenticate/activate-code`,
  SIGN_IN_GOOGLE: `${userVersion}/authenticate/google`,
  FORGOT_PASSWORD: `${userVersion}/authenticate/forgot-password`,
  VERIFY_CODE: `${userVersion}/authenticate/forgot-password/verify-code`,
  RESET_PASSWORD: `${userVersion}/authenticate/forgot-password/reset`,
}

export default class UserAPI {
  static signIn = (payload) => appAxios.post(API_USER.SIGN_IN, payload)
  static changePassword = (payload) =>
    appAxios.post(API_USER.CHANGE_PASSWORD, payload)

  static register = (payload) => appAxios.post(API_USER.REGISTER, payload)

  static activeAccount = (payload) =>
    appAxios.post(API_USER.ACTIVE_ACCOUNT, payload)

  static signInGoogle = (payload) =>
    appAxios.post(API_USER.SIGN_IN_GOOGLE, payload)

  static forgotPassword = (payload) =>
    appAxios.post(API_USER.FORGOT_PASSWORD, payload)

  static verifyCode = (payload) => appAxios.post(API_USER.VERIFY_CODE, payload)
  static resetPassword = (payload) =>
    appAxios.post(API_USER.RESET_PASSWORD, payload)
}
