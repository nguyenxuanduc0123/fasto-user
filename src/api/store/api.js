import appAxios from '../constant/axios-config'
import { userVersion } from '../constant/version'

export const API_STORE = {
  GET_LIST_STORE: `${userVersion}/management/shop/shops`,
  GET_TOP_STORE: `${userVersion}/management/shop/top-shops`,
  GET_DISTANCE_STORE: `${userVersion}/management/shop/distance`,
  GET_DISTANCE_AROUND_STORE: `${userVersion}/management/shop/distance-shop`,
  GET_DETAIL_STORE: (id) => `${userVersion}/management/shop/detail/${id}`,
  GET_FAVORITE_STORE: `${userVersion}/management/shops`,
  ADD_FAVORITE_STORE: (id) => `${userVersion}/management/shops/${id}`,
  REMOVE_FAVORITE_STORE: (id) => `${userVersion}/management/shops/${id}`,
}

export default class StoreAPI {
  static getListStore = (params = {}) =>
    appAxios.get(API_STORE.GET_LIST_STORE, params)

  static getTopStore = (params = {}) =>
    appAxios.get(API_STORE.GET_TOP_STORE, params)

  static getDistanceStore = (params = {}) =>
    appAxios.get(API_STORE.GET_DISTANCE_STORE, params)

  static getDistanceAroundStore = (params = {}) =>
    appAxios.get(API_STORE.GET_DISTANCE_AROUND_STORE, params)

  static getListFavoriteStore = (params = {}) =>
    appAxios.get(API_STORE.GET_FAVORITE_STORE, params)

  static getDetailStore = (params = {}) =>
    appAxios.get(API_STORE.GET_DETAIL_STORE(params.id), params)

  static addFavoriteStore = (params = {}) =>
    appAxios.post(API_STORE.ADD_FAVORITE_STORE(params.id), params)

  static removeFavoriteStore = (params = {}) =>
    appAxios.delete(API_STORE.REMOVE_FAVORITE_STORE(params.id), params)
}
