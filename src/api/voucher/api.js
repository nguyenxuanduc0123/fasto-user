import appAxios from '../constant/axios-config'
import { userVersion } from '../constant/version'

export const API_VOUCHER = {
  GET_LIST_VOUCHER: `${userVersion}/management/vouchers`,
  GET_DETAIL_VOUCHER: (id) => `${userVersion}/management/vouchers/${id}`,
  GET_LIST_VOUCHER_BILL: `${userVersion}/management/vouchers/bill`,
}

export default class VoucherAPI {
  static getListVoucher = (params = {}) =>
    appAxios.get(API_VOUCHER.GET_LIST_VOUCHER, params)

  static getDetailVoucher = (params = {}) =>
    appAxios.get(API_VOUCHER.GET_DETAIL_VOUCHER(params.id), params)

  static getListVoucherBill = (params = {}) =>
    appAxios.post(API_VOUCHER.GET_LIST_VOUCHER_BILL, params)
}
