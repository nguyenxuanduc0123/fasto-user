import appAxios from '../constant/axios-config'
import { userVersion } from '../constant/version'

export const API_CART = {
  ADD_TO_CART: `${userVersion}/management/carts`,
  GET_LIST_CART: `${userVersion}/management/carts`,
}

export default class CartAPI {
  static addToCart = (params = {}) =>
    appAxios.post(API_CART.ADD_TO_CART, params)

  static getListCart = (params = {}) =>
    appAxios.get(API_CART.GET_LIST_CART, params)
}
