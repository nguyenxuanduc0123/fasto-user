import * as yup from 'yup'

export const phoneRegExp =
  /^0(([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

export const emailRegExp =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const registerSchema = yup.object().shape({
  email: yup
    .string()
    .trim()
    .matches(emailRegExp, 'Invalid email address')
    .required('Email is required'),
  password: yup
    .string()
    .trim()
    .min(8, 'Should be 8 chars minimum')
    .required('Password is required'),
  phoneNumber: yup
    .string()
    .matches(phoneRegExp, 'Phone number is not valid')
    .min(10)
    .required('Please enter your mobile')
    .trim(),
  firstname: yup.string().trim().required('First name is required'),
  lastname: yup.string().trim().required('Last name is required'),
  birthDay: yup.string().trim().required('Birthday is required'),
})

export { registerSchema }
